﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using MagentaToSAPIntegration.Models;
using SAPbobsCOM;

namespace MagentaToSAPIntegration.Services
{
    class DataTransform
    {
        private SQLServerDataDataContext dataContext = new SQLServerDataDataContext();
        string ExtendedLogging = Properties.Settings.Default.ExtendedLogging == null ? "N" : Properties.Settings.Default.ExtendedLogging;

        public List<RecordProduct> SAPProductsToMagenta(DateTime dated)
        {
            List<RecordProduct> ListMagProds = new List<RecordProduct>();

            try
            {
                Program.log.Info("Begin GET SAP B1 Process: Products");
                dataContext.sp_pricetracking();

                var query = from vPRODUCT in dataContext.vPRODUCTs
                        where vPRODUCT.LastSAPUpdate >= dated
                        || vPRODUCT.BarcodeLastSAPUpdate >= dated
                        || vPRODUCT.CostLastSAPUpdate >= dated
                        || vPRODUCT.PriceLastSAPUpdate >= dated
                        select vPRODUCT;

                foreach (vPRODUCT sapprod in query)
                {
                    if (ExtendedLogging == "Y")
                        Program.log.Info(string.Format("Building JSON Product:{0}, for Updating Magenta", sapprod.ProductCode));

                    RecordProduct MagProd = new RecordProduct();
                    MagProd.ProductCode = sapprod.ProductCode;
                    MagProd.Description = sapprod.ShortDescription;
                    MagProd.Inactive = sapprod.Inactive == "TRUE" ? true : false;
                    MagProd.Purge = sapprod.Purge == "TRUE" ? true : false;
                    MagProd.NonInventory = sapprod.NonInventory == "TRUE" ? true : false;
                    MagProd.SerialNumberRequired = sapprod.SerialNumberRequired == "TRUE" ? true : false;
                    MagProd.Analysis1Code = sapprod.Analysis1Code.ToString();
                    MagProd.Analysis2Code = sapprod.Analysis2Code.ToString();
                    MagProd.Analysis3Code = sapprod.Analysis3Code.ToString();
                    MagProd.Analysis4Code = sapprod.Analysis4Code.ToString();
                    MagProd.Supplier1 = sapprod.Supplier1;
                    MagProd.SupplierCode1 = sapprod.SupplierCode1;
                    MagProd.ExternalId1 = sapprod.ExternalId1;
                    MagProd.ExtraInfo1 = sapprod.ExtraInfo1;
                    //MagProd.StockedInWarehouse = sapprod.StockedInWarehouse;

                    if (sapprod.OverrideTaxRate == "true")
                    {
                        MagProd.OverrideTaxRate = true;
                        MagProd.Taxrate = Convert.ToDouble(sapprod.Taxrate);
                    }
                    else
                    {
                        MagProd.OverrideTaxRate = false;
                    }

                    MagProd.BranchSetPrices = SAPBranchPriceToMagentaByProduct(sapprod.ProductCode);
                    MagProd.CostsArea = SAPCostAreaToMagentaByProduct(sapprod.ProductCode);
                    MagProd.Barcodes = SAPBarcodesToMagentaByProduct(sapprod.ProductCode);
                    MagProd.Websites = SAPWebsitesToMagentaByProduct(sapprod.ProductCode);

                    ListMagProds.Add(MagProd);
                }

                Program.log.Info("End GET SAP B1 Process: Products");
            }
            catch (Exception ex)
            {
                Program.log.Error(ex);
            }

            return ListMagProds;
        }

        public List<RecordBranchSetPrice> SAPBranchPriceToMagentaByProduct(string product)
        {
            List<RecordBranchSetPrice> ListMagProdPrices = new List<RecordBranchSetPrice>();

            try
            {
                var query = from vPRODUCTPRICE in dataContext.vPRODUCTPRICEs
                            where vPRODUCTPRICE.ProductCode == product
                            select vPRODUCTPRICE;

                foreach (vPRODUCTPRICE sapprod in query)
                {
                    RecordBranchSetPrice MagProdPrice = new RecordBranchSetPrice();
                    MagProdPrice.BranchSetCode = sapprod.BranchSetCode;
                    MagProdPrice.RRP = Convert.ToDouble(sapprod.RRP);
                    MagProdPrice.RetailPrice = Convert.ToDouble(sapprod.RetailPrice);
                    MagProdPrice.FullPrice = Convert.ToDouble(sapprod.FullPrice);

                    ListMagProdPrices.Add(MagProdPrice);
                }
            }
            catch (Exception ex)
            {
                Program.log.Error(ex);
            }

            return ListMagProdPrices;
        }

        public List<RecordCostsArea> SAPCostAreaToMagentaByProduct(string product)
        {
            List<RecordCostsArea> ListMagCostsArea = new List<RecordCostsArea>();

            try
            {
                var query = from vPRODUCTCOST in dataContext.vPRODUCTCOSTs
                            where vPRODUCTCOST.ProductCode == product
                            select vPRODUCTCOST;

                foreach (vPRODUCTCOST sapprod in query)
                {
                    RecordCostsArea MagProdCost = new RecordCostsArea();
                    MagProdCost.AreaCode = sapprod.AreaCode;
                    MagProdCost.AreaType = Convert.ToInt32(sapprod.AreaType);
                    MagProdCost.BuyPrice1 = Convert.ToDouble(sapprod.BuyPrice1);

                    ListMagCostsArea.Add(MagProdCost);
                }

            }
            catch (Exception ex)
            {
                Program.log.Error(ex);
            }

            return ListMagCostsArea;
        }

        public List<RecordBarcode> SAPBarcodesToMagentaByProduct(string product)
        {
            List<RecordBarcode> ListMagProds = new List<RecordBarcode>();

            try
            {
                var query = from vBARCODE in dataContext.vBARCODEs
                            where vBARCODE.ProductCode == product
                            select vBARCODE;

                foreach (vBARCODE sapprod in query)
                {
                    RecordBarcode MagBarcode = new RecordBarcode();
                    MagBarcode.NewBarcode = sapprod.NewBarcode;

                    ListMagProds.Add(MagBarcode);
                }
            }
            catch (Exception ex)
            {
                Program.log.Error(ex);
            }

            return ListMagProds;
        }

        public List<RecordWebsites> SAPWebsitesToMagentaByProduct(string product)
        {
            List<RecordWebsites> ListMagProds = new List<RecordWebsites>();

            try
            {
                var query = from vPRODUCTWEBSITE in dataContext.vPRODUCTWEBSITEs
                            where vPRODUCTWEBSITE.ProductCode == product
                            select vPRODUCTWEBSITE;

                foreach (vPRODUCTWEBSITE sapprod in query)
                {
                    RecordWebsites MagWebsites = new RecordWebsites();
                    MagWebsites.WebsiteCode = sapprod.WebsiteCode;
                    MagWebsites.Publish = sapprod.Publish == "TRUE" ? true : false;

                    ListMagProds.Add(MagWebsites);
                }
            }
            catch (Exception ex)
            {
                Program.log.Error(ex);
            }

            return ListMagProds;
        }

        public List<RecordBranchSetPrice> SAPBranchPriceToMagenta(DateTime dated)
        {
            List<RecordBranchSetPrice> ListMagProdPrices = new List<RecordBranchSetPrice>();

            try
            {
                Program.log.Info("Begin GET SAP B1 Process: Branch Price");

                var query = from vPRODUCTPRICE in dataContext.vPRODUCTPRICEs
                            where vPRODUCTPRICE.LastSAPUpdate >= dated
                            select vPRODUCTPRICE;

                foreach (vPRODUCTPRICE sapprod in query)
                {
                    RecordBranchSetPrice MagProdPrice = new RecordBranchSetPrice();
                    //MagProdPrice.ProductCode = sapprod.ProductCode;
                    MagProdPrice.BranchSetCode = sapprod.BranchSetCode;
                    MagProdPrice.RRP = Convert.ToDouble(sapprod.RRP);
                    MagProdPrice.RetailPrice = Convert.ToDouble(sapprod.RetailPrice);
                    MagProdPrice.FullPrice = Convert.ToDouble(sapprod.FullPrice);

                    ListMagProdPrices.Add(MagProdPrice);
                }

                Program.log.Info("End GET SAP B1 Process: Branch Price");
            }
            catch (Exception ex)
            {
                Program.log.Error(ex);
            }

            return ListMagProdPrices;
        }

        public List<RecordCostsArea> SAPCostAreaToMagenta(DateTime dated)
        {
            List<RecordCostsArea> ListMagCostsArea = new List<RecordCostsArea>();

            try
            {
                Program.log.Info("Begin GET SAP B1 Process: Costs Area");

                var query = from vPRODUCTCOST in dataContext.vPRODUCTCOSTs
                            where vPRODUCTCOST.LastSAPUpdate >= dated
                            select vPRODUCTCOST;

                foreach (vPRODUCTCOST sapprod in query)
                {
                    RecordCostsArea MagProdCost = new RecordCostsArea();
                    //MagProdCost.ProductCode = sapprod.ProductCode;
                    MagProdCost.AreaCode = sapprod.AreaCode;
                    MagProdCost.AreaType = Convert.ToInt32(sapprod.AreaType);
                    MagProdCost.BuyPrice1 = Convert.ToDouble(sapprod.BuyPrice1);

                    ListMagCostsArea.Add(MagProdCost);
                }

                Program.log.Info("End GET SAP B1 Process: Costs Area");
            }
            catch (Exception ex)
            {
                Program.log.Error(ex);
            }

            return ListMagCostsArea;
        }

        public List<RecordBarcode> SAPBarcodesToMagenta(DateTime dated)
        {
            List<RecordBarcode> ListMagProds = new List<RecordBarcode>();

            try
            {
                Program.log.Info("Begin GET SAP B1 Process: Barcodes");

                var query = from vBARCODE in dataContext.vBARCODEs
                            where vBARCODE.LastSAPUpdate >= dated
                            select vBARCODE;

                foreach (vBARCODE sapprod in query)
                {
                    RecordBarcode MagBarcode = new RecordBarcode();
                    //MagBarcode.ProductCode = sapprod.ProductCode;
                    MagBarcode.NewBarcode = sapprod.NewBarcode;

                    ListMagProds.Add(MagBarcode);
                }

                Program.log.Info("End GET SAP B1 Process: Barcodes");
            }
            catch (Exception ex)
            {
                Program.log.Error(ex);
            }

            return ListMagProds;
        }

        public List<RecordSupplier> SAPSuppliersToMagenta(DateTime dated)
        {
            List<RecordSupplier> ListMagSuppliers = new List<RecordSupplier>();

            try
            {
                Program.log.Info("Begin GET SAP B1 Process: Suppliers");

                var query = from vSUPPLIER in dataContext.vSUPPLIERs
                        where vSUPPLIER.LastSAPUpdate >= dated
                        select vSUPPLIER;

                foreach (vSUPPLIER sapsupp in query)
                {
                    RecordSupplier MagSupp = new RecordSupplier();
                    MagSupp.SupplierCode = sapsupp.SupplierCode;
                    MagSupp.Name = sapsupp.Name;
                    MagSupp.Address1 = sapsupp.Address1;
                    MagSupp.Address2 = sapsupp.Address2;
                    MagSupp.Address3 = sapsupp.Address3;
                    MagSupp.Address4 = sapsupp.Address4;
                    MagSupp.Postcode = sapsupp.Postcode;
                    MagSupp.State = sapsupp.State;
                    MagSupp.Email = sapsupp.Email;
                    MagSupp.Website = sapsupp.Website;
                    MagSupp.Phone = sapsupp.Phone;
                    MagSupp.Fax = sapsupp.Fax;

                    ListMagSuppliers.Add(MagSupp);
                }

                Program.log.Info("End GET SAP B1 Process: Suppliers");
            }
            catch (Exception ex)
            {
                Program.log.Error(ex);
            }

            return ListMagSuppliers;
        }

        public List<RecordAnalysisCode1> SAPAnalysisCode1ToMagenta()
        {
            List<RecordAnalysisCode1> ListAnalysisCode1 = new List<RecordAnalysisCode1>();

            try
            {
                Program.log.Info("Begin GET SAP B1 Process: AnalysisCode1");

                var query = from vANALYSISCODE in dataContext.vANALYSISCODEs
                        where vANALYSISCODE.FieldRef == "AnalysisCode1"
                        select vANALYSISCODE;

                foreach (vANALYSISCODE sapcode1 in query)
                {
                    RecordAnalysisCode1 Magcode1 = new RecordAnalysisCode1();
                    Magcode1.Analysis1code = sapcode1.Analysiscode;
                    Magcode1.Description = sapcode1.Description;

                    ListAnalysisCode1.Add(Magcode1);
                }

                Program.log.Info("End GET SAP B1 Process: AnalysisCode1");
            }
            catch (Exception ex)
            {
                Program.log.Error(ex);
            }

            return ListAnalysisCode1;
        }

        public List<RecordAnalysisCode2> SAPAnalysisCode2ToMagenta()
        {
            List<RecordAnalysisCode2> ListAnalysisCode2 = new List<RecordAnalysisCode2>();

            try
            {
                Program.log.Info("Begin GET SAP B1 Process: AnalysisCode2");

                var query = from vANALYSISCODE in dataContext.vANALYSISCODEs
                            where vANALYSISCODE.FieldRef == "AnalysisCode2"
                            select vANALYSISCODE;

                foreach (vANALYSISCODE sapcode2 in query)
                {
                    RecordAnalysisCode2 Magcode2 = new RecordAnalysisCode2();
                    Magcode2.Analysis2code = sapcode2.Analysiscode;
                    Magcode2.Description = sapcode2.Description;

                    ListAnalysisCode2.Add(Magcode2);
                }

                Program.log.Info("End GET SAP B1 Process: AnalysisCode2");
            }
            catch (Exception ex)
            {
                Program.log.Error(ex);
            }

            return ListAnalysisCode2;
        }

        public List<RecordAnalysisCode3> SAPAnalysisCode3ToMagenta()
        {
            List<RecordAnalysisCode3> ListAnalysisCode3 = new List<RecordAnalysisCode3>();

            try
            {
                Program.log.Info("Begin GET SAP B1 Process: AnalysisCode3");

                var query = from vANALYSISCODE in dataContext.vANALYSISCODEs
                            where vANALYSISCODE.FieldRef == "AnalysisCode3"
                            select vANALYSISCODE;

                foreach (vANALYSISCODE sapcode3 in query)
                {
                    RecordAnalysisCode3 Magcode3 = new RecordAnalysisCode3();
                    Magcode3.Analysis3code = sapcode3.Analysiscode;
                    Magcode3.Description = sapcode3.Description;

                    ListAnalysisCode3.Add(Magcode3);
                }

                Program.log.Info("End GET SAP B1 Process: AnalysisCode3");
            }
            catch (Exception ex)
            {
                Program.log.Error(ex);
            }

            return ListAnalysisCode3;
        }

        public List<RecordAnalysisCode4> SAPAnalysisCode4ToMagenta()
        {
            List<RecordAnalysisCode4> ListAnalysisCode4 = new List<RecordAnalysisCode4>();

            try
            {
                Program.log.Info("Begin GET SAP B1 Process: AnalysisCode4");

                var query = from vANALYSISCODE in dataContext.vANALYSISCODEs
                            where vANALYSISCODE.FieldRef == "AnalysisCode4"
                            select vANALYSISCODE;

                foreach (vANALYSISCODE sapcode4 in query)
                {
                    RecordAnalysisCode4 Magcode4 = new RecordAnalysisCode4();
                    Magcode4.Analysis4code = sapcode4.Analysiscode;
                    Magcode4.Description = sapcode4.Description;

                    ListAnalysisCode4.Add(Magcode4);
                }

                Program.log.Info("End GET SAP B1 Process: AnalysisCode4");
            }
            catch (Exception ex)
            {
                Program.log.Error(ex);
            }

            return ListAnalysisCode4;
        }

        public List<RecordCustomerToMagenta> SAPCustomerToMagenta(DateTime dated)
        {
            List<RecordCustomerToMagenta> ListCustomerToMagenta = new List<RecordCustomerToMagenta>();

            try
            {
                Program.log.Info("Begin GET SAP B1 Process: CustomerToMagenta");

                dataContext.sp_BPCustomerCodeUpdate(dated);

                var query = from vCUSTOMER in dataContext.vCUSTOMERs
                            where vCUSTOMER.U_INE_ExportToMagenta == "Y"
                                && vCUSTOMER.LastSAPUpdate >= dated
                            select vCUSTOMER;

                foreach (vCUSTOMER sapcust in query)
                {
                    RecordCustomerToMagenta MagCust = new RecordCustomerToMagenta();
                    MagCust.CustomerCode = sapcust.U_MagCustomerCode;
                    MagCust.Surname = sapcust.Surname;
                    MagCust.Firstname = sapcust.Firstname;
                    MagCust.Address1 = sapcust.Address1;
                    MagCust.Address2 = sapcust.Address2;
                    MagCust.Address3 = sapcust.Address3;
                    MagCust.Postcode = sapcust.Postcode;
                    MagCust.State = sapcust.State;
                    MagCust.Delivery1 = sapcust.Delivery1;
                    MagCust.Delivery2 = sapcust.Delivery2;
                    MagCust.Delivery3 = sapcust.Delivery3;
                    MagCust.DeliveryPostcode = sapcust.DeliveryPostcode;
                    MagCust.DeliveryState = sapcust.DeliveryState;
                    MagCust.Email = sapcust.Email;
                    MagCust.Phone = sapcust.Phone;
                    MagCust.Phone1 = sapcust.Phone1;
                    MagCust.Mobile = sapcust.Mobile;
                    MagCust.OptOutEmail = sapcust.U_MagEmailOptOut == "TRUE" ? true : false;

                    ListCustomerToMagenta.Add(MagCust);
                }

                Program.log.Info("End GET SAP B1 Process: CustomerToMagenta");
            }
            catch (Exception ex)
            {
                Program.log.Error(ex);
            }

            return ListCustomerToMagenta;
        }

        public BPCustomer SAPCustomer(MagentaCustomer MagCustomer, string BatchID = "", string TranID = "", string TranType = "", BPCustomer CustOld = null)
        {
            BPCustomer Cust = new BPCustomer();
            string CustName = string.Format("{0} {1}", MagCustomer.Firstname, MagCustomer.Surname);
            string OptOutEmail = "";
            string OptOutMobile = "";
            string ChargeAccount = "";
            string HasCreditLimit = "";
            string HasTransactionLimit = "";
            string NotExceedCreditLimit = "";
            string Company = string.IsNullOrEmpty(MagCustomer.Company) ? "" : MagCustomer.Company;
            string CustNameAddr = string.Format("{0} {1}", MagCustomer.Firstname, MagCustomer.Surname);
            string Address1 = string.IsNullOrEmpty(MagCustomer.Address1) ? MagCustomer.AddressLine1 : MagCustomer.Address1;
            string Address2 = string.IsNullOrEmpty(MagCustomer.Address2) ? MagCustomer.AddressLine2 : MagCustomer.Address2;
            string Address3 = string.IsNullOrEmpty(MagCustomer.Address3) ? MagCustomer.AddressLine3 : MagCustomer.Address3;
            string State = MagCustomer.State;
            string Postcode = string.IsNullOrEmpty(MagCustomer.Postcode) ? MagCustomer.PostCode : MagCustomer.Postcode;
            string firstname = MagCustomer.Firstname;
            string surname = MagCustomer.Surname;
            string nametrunc = "N";

            if (CustName.Length > 100)
            {
                CustName = CustName.Substring(0, 100);
                nametrunc = "Y";
            }

            if (CustNameAddr.Length > 50)
            {
                CustNameAddr = CustNameAddr.Substring(0, 50);
                nametrunc = "Y";
            }


            switch (MagCustomer.OptOutEmail)
            {
                case "1":
                    OptOutEmail = "Y";
                    break;
                case "0":
                    OptOutEmail = "N";
                    break;
                case "Y":
                    OptOutEmail = "Y";
                    break;
                case "N":
                    OptOutEmail = "N";
                    break;
            }

            switch (MagCustomer.OptOutMobile)
            {
                case "1":
                    OptOutMobile = "Y";
                    break;
                case "0":
                    OptOutMobile = "N";
                    break;
                case "Y":
                    OptOutMobile = "Y";
                    break;
                case "N":
                    OptOutMobile = "N";
                    break;
            }

            switch (MagCustomer.ChargeAccount)
            {
                case "1":
                    ChargeAccount = "Y";
                    break;
                case "0":
                    ChargeAccount = "N";
                    break;
                case "Y":
                    ChargeAccount = "Y";
                    break;
                case "N":
                    ChargeAccount = "N";
                    break;
            }

            switch (MagCustomer.HasCreditLimit)
            {
                case "1":
                    HasCreditLimit = "Y";
                    break;
                case "0":
                    HasCreditLimit = "N";
                    break;
                case "Y":
                    HasCreditLimit = "Y";
                    break;
                case "N":
                    HasCreditLimit = "N";
                    break;
            }

            switch (MagCustomer.HasTransactionLimit)
            {
                case "1":
                    HasTransactionLimit = "Y";
                    break;
                case "0":
                    HasTransactionLimit = "N";
                    break;
                case "Y":
                    HasTransactionLimit = "Y";
                    break;
                case "N":
                    HasTransactionLimit = "N";
                    break;
            }

            switch (MagCustomer.NotExceedCreditLimit)
            {
                case "1":
                    NotExceedCreditLimit = "Y";
                    break;
                case "0":
                    NotExceedCreditLimit = "N";
                    break;
                case "Y":
                    NotExceedCreditLimit = "Y";
                    break;
                case "N":
                    NotExceedCreditLimit = "N";
                    break;
            }

            dataContext.sp_BPCustomer(CustName, MagCustomer.Email, MagCustomer.Phone, MagCustomer.Phone1, MagCustomer.Mobile,
                Company, CustNameAddr, MagCustomer.Delivery1, MagCustomer.Delivery2, "", MagCustomer.Delivery3, MagCustomer.DeliveryState, "", MagCustomer.DeliveryPostcode, MagCustomer.Mobile, "",
                Company, CustNameAddr, Address1, Address2, "", Address3, MagCustomer.State, "", Postcode, MagCustomer.Mobile, "",
                MagCustomer.CustomerCode, OptOutEmail, OptOutMobile, "",
                MagCustomer.CustomerCategoryCode, ChargeAccount, MagCustomer.AccountStatus, HasCreditLimit, MagCustomer.CreditLimit, MagCustomer.NewCreditLimit,
                HasTransactionLimit, MagCustomer.TransactionLimit, MagCustomer.NewTransactionLimit, NotExceedCreditLimit, MagCustomer.InvoiceDueDays, MagCustomer.InvoiceDueMode,
                BatchID, TranID, TranType, firstname, surname, nametrunc);

            return Cust;
        }

        public string SAPOrderTransactions(Sh SHDetail, string BatchID, string TranID)
        {
            string SHType = "";
            string ID = SHDetail.invoicenumber;
            ARInvoicesHDR InvHDR = new ARInvoicesHDR();
            List<ARInvoicesLINE> InvLines = new List<ARInvoicesLINE>();
            ARCreditMemosHDR CreditHDR = new ARCreditMemosHDR();
            List<ARCreditMemosLINE> CreditLines = new List<ARCreditMemosLINE>();
            ARPreOrdersHDR OrdHDR = new ARPreOrdersHDR();
            List<ARPreOrdersLINE> OrdLines = new List<ARPreOrdersLINE>();
            List<PaymentsHDR> PayHDRs = new List<PaymentsHDR>();
            List<PaymentsLINE> PayLines = new List<PaymentsLINE>();

            // check for vouchers which will make the transaction a positive value, allowing invoice processing rather than credit processing
            double checktotal = 0;
            double tendertotal = 0;
            double vouchtotal = 0;
            List<Tn> CheckTender = new List<Tn>();
            List<Oc> CheckVoucher = new List<Oc>();
            CheckVoucher = SHDetail.oc;
            CheckTender = SHDetail.tn;

            Console.WriteLine(ID);

            if (CheckVoucher != null && CheckTender != null && CheckVoucher.Count() > 0 && SHDetail.totalgstinclusive < 0)
            {
                foreach (Tn tline in CheckTender)
                {
                    if (tline.tendercode == "GC")
                    {
                        tendertotal = tendertotal + tline.amount;
                    }
                }

                foreach (Oc vline in CheckVoucher)
                {
                    vouchtotal = vouchtotal + vline.issueamount;
                }

                checktotal = SHDetail.totalgstinclusive + vouchtotal + tendertotal;
            }
            else
            {
                checktotal = SHDetail.totalgstinclusive;
            }

            if (checktotal > 0 && SHDetail.sl != null && SHDetail.sl.Count > 0 && SHDetail.oh == null && SHDetail.ot == null)
                SHType = "INVOICE";
            else if (checktotal < 0 && SHDetail.sl != null && SHDetail.sl.Count > 0 && SHDetail.oh == null && SHDetail.ot == null)
                SHType = "CREDIT";
            else if (checktotal == 0 && SHDetail.sl != null && SHDetail.sl.Count > 0 && SHDetail.oh == null && SHDetail.ot == null)
                SHType = "EXCHANGE";
            else if (checktotal == 0 && (SHDetail.sl == null || SHDetail.sl.Count == 0) && SHDetail.oh != null && SHDetail.ot != null)
                SHType = "LAYBY";
            else if (checktotal >= 0 && (SHDetail.sl != null && SHDetail.sl.Count > 0) && SHDetail.oh != null && SHDetail.ot != null)
                SHType = "LAYBYINVOICE";
            else if (checktotal == 0 && (SHDetail.sl == null || SHDetail.sl.Count == 0) && SHDetail.oh == null && SHDetail.ot == null && SHDetail.oc == null)
                SHType = "PAYMENTONLY";
            else if (checktotal == 0 && (SHDetail.sl == null || SHDetail.sl.Count == 0) && SHDetail.oh != null && SHDetail.ot == null && SHDetail.oc == null && SHDetail.tn != null)
                SHType = "PAYMENTONLY";
            else if (checktotal == 0 && (SHDetail.sl == null || SHDetail.sl.Count == 0) && SHDetail.oh == null && SHDetail.ot == null && SHDetail.oc != null)
                SHType = "GIFTCARDONLY";

            var tranquery = dataContext.fn_TranID(SHType);
            int BASETRANID = tranquery.GetValueOrDefault();
            BASETRANID++;
            string WHID;
            string CustCode;
            int? PAYTRANIDNULL;
            int PAYTRANID;
            string TranAccount;
            string Taxcode = "S1";
            string TaxFreecode = "S2";
            string Currency = "AUD";
            int changecountrec = 0;
            decimal vouchertotal = 0;
            string OrderSubType = "";
            bool OrderSubTypeLine1 = true;
            decimal changeapportioned = 0;

            switch (SHType)
            {
                case "INVOICE":
                case "EXCHANGE":
                    WHID = dataContext.fn_SAPWHCode(SHDetail.branchcode).ToString();
                    CustCode = dataContext.fn_SAPCardCode(SHDetail.customernumber).ToString();

                    foreach (Sl Line in SHDetail.sl)
                    {
                        ARInvoicesLINE InvLine = new ARInvoicesLINE();
                        InvLine.OrderID = SHDetail.invoicenumber;
                        InvLine.OriginalOrderID = Line.originalinvoicenumber;
                        InvLine.ItemCode = Line.plu;
                        InvLine.Quantity = Convert.ToDecimal(Line.quantity);
                        InvLine.HDRID = BASETRANID;
                        InvLine.UnitPrice = Convert.ToDecimal(Line.unitpriceinclgst);

                        if (Line.gstrate == 0)
                        {
                            InvLine.TaxCode = TaxFreecode;
                        }
                        else
                        {
                            InvLine.TaxCode = Taxcode;
                        }

                        InvLine.WarehouseCode = WHID;
                        InvLine.U_MagInvoiceNumber = Line.invoicenumber;
                        InvLine.U_MagLineNumber = Line.linenumber;
                        InvLine.U_MagReturnCode = Line.returnreasoncode;
                        InvLine.U_MagDiscountReasonCode = Line.discountreasoncode;
                        InvLine.U_MagDiscountSubReasonCode = Line.discountsubreasoncode;
                        InvLine.U_MagPromoCode = Line.promotioncode;
                        InvLine.U_MagTranID = TranID;
                        if (SHDetail.Sixty != null)
                        {
                            InvLine.U_MagOrigInvNumber = SHDetail.Sixty.Where(x => x.linenumber == Line.linenumber).FirstOrDefault() == null ? "" : SHDetail.Sixty.Where(x => x.linenumber == Line.linenumber).FirstOrDefault().originalinvoicenumber;
                            InvLine.U_MagOrigLineNumber = SHDetail.Sixty.Where(x => x.linenumber == Line.linenumber).FirstOrDefault() == null ? -1 : InvLine.U_MagOrigLineNumber = SHDetail.Sixty.Where(x => x.linenumber == Line.linenumber).FirstOrDefault().originalsaleline;
                            InvLine.U_MagReturnDetail = SHDetail.Sixty.Where(x => x.linenumber == Line.linenumber).FirstOrDefault() == null ? "" : SHDetail.Sixty.Where(x => x.linenumber == Line.linenumber).FirstOrDefault().returnreasonextradetail;
                        }

                        // Insert Invoice Lines
                        dataContext.ARInvoicesLINEs.InsertOnSubmit(InvLine);
                        dataContext.SubmitChanges();
                    }

                    if (SHType != "EXCHANGE" && SHDetail.tn != null)
                    {
                        PAYTRANIDNULL = dataContext.fn_TranID("PAYMENT");
                        PAYTRANID = PAYTRANIDNULL ?? 0;
                        vouchertotal = 0;
                        changecountrec = 0;
                        changeapportioned = 0;

                        if (SHDetail.oc != null)
                        {
                            foreach (Oc Voucher in SHDetail.oc)
                            {
                                ARInvoicesLINE VoucherInvLine = new ARInvoicesLINE();
                                VoucherInvLine.OrderID = SHDetail.invoicenumber;
                                VoucherInvLine.ItemCode = "VOUCHER";
                                VoucherInvLine.Quantity = 1;
                                VoucherInvLine.HDRID = BASETRANID;
                                VoucherInvLine.UnitPrice = Convert.ToDecimal(Voucher.issueamount);
                                VoucherInvLine.TaxCode = TaxFreecode;
                                VoucherInvLine.WarehouseCode = WHID;
                                VoucherInvLine.U_MagInvoiceNumber = SHDetail.invoicenumber;
                                VoucherInvLine.U_MagVoucherCode = Voucher.vouchernumber;
                                VoucherInvLine.U_MagTranID = TranID;

                                if (Voucher.issueamount < 0)
                                {
                                    VoucherInvLine.U_MagVoucherAmt = Convert.ToDecimal(Voucher.issueamount * -1);
                                    VoucherInvLine.UnitPrice = Convert.ToDecimal(Voucher.issueamount * -1);
                                }
                                else
                                {
                                    VoucherInvLine.U_MagVoucherAmt = Convert.ToDecimal(Voucher.issueamount);
                                    VoucherInvLine.UnitPrice = Convert.ToDecimal(Voucher.issueamount);
                                }

                                // Insert Invoice Lines
                                dataContext.ARInvoicesLINEs.InsertOnSubmit(VoucherInvLine);
                                dataContext.SubmitChanges();
                            }
                        }

                        foreach (Tn Tender in SHDetail.tn)
                        {
                            if (Tender.changerecords == true)
                                changecountrec++;
                        }


                        foreach (Tn Tender in SHDetail.tn)
                        {
                            if (Tender.changerecords == false)
                            {
                                decimal tenderamount = Convert.ToDecimal(Tender.amount);
                                decimal change = Convert.ToDecimal(Tender.changeapportionedtothistender);

                                if (Tender.tendercode == "CASH")
                                {
                                    if (changeapportioned <= change)
                                    {
                                        if (tenderamount > (change - changeapportioned))
                                        {
                                            decimal tranchange = (change - changeapportioned);
                                            tenderamount = Convert.ToDecimal(tenderamount - tranchange);
                                            changeapportioned = (changeapportioned + tranchange);
                                        }
                                        else
                                        {
                                            changeapportioned = (changeapportioned + tenderamount);
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        tenderamount = Convert.ToDecimal(Tender.amount);
                                    }
                                }
                                else
                                {
                                    tenderamount = Convert.ToDecimal(Tender.amount);
                                }

                                if (tenderamount > 0 && Tender.tendercode != "ACCOUNT")
                                {
                                    TranAccount = dataContext.fn_PaymentAccount(Tender.tendercode, WHID);
                                    PAYTRANID++;
                                    PaymentsHDR PayHDR = new PaymentsHDR();
                                    PaymentsLINE PayLine = new PaymentsLINE();
                                    PayHDR.ID = PAYTRANID;
                                    PayHDR.CardCode = CustCode;
                                    PayHDR.CreateDate = DateTime.ParseExact(Tender.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                                    PayHDR.DocDate = DateTime.ParseExact(Tender.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                                    PayHDR.TransferDate = DateTime.ParseExact(Tender.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                                    PayHDR.TransferAccount = TranAccount;
                                    PayHDR.OrderID = SHDetail.invoicenumber;
                                    PayHDR.TransferSum = tenderamount;
                                    PayHDR.U_MagBatchID = BatchID;
                                    PayHDR.U_MagTranID = TranID;
                                    PayHDR.U_MagTranNumber = Tender.transactionnumber;
                                    PayHDR.U_MagInvoiceNumber = Tender.invoicenumber;
                                    PayHDR.U_MagPaymentMethod = Tender.tendercode;
                                    PayHDR.U_MagCustomerCode = SHDetail.customernumber;
                                    PayHDR.U_MagHosFileRef = SHDetail.numberofhosfilerows;
                                    PayHDR.U_MagTNChangeCount = changecountrec;
                                    PayHDR.U_MagLineNumber = Tender.tenderlinenumber;
                                    PayHDR.IN_OUT = "IN";
                                    PayHDR.Status = "Queued";
                                    PayHDR.StatusDate = DateTime.Now;
                                    PayHDR.StatusMessage = "";

                                    if (Tender.tendercode == "GC")
                                        PayHDR.U_MagVoucherCode = Tender.refnumber;

                                    PayLine.HDRID = PAYTRANID;
                                    PayLine.OrderID = SHDetail.invoicenumber;
                                    PayLine.InvoiceType = "it_Invoice";
                                    PayLine.SumApplied = tenderamount;
                                    PayLine.U_MagTranID = TranID;
                                    PayLine.U_MagLineNumber = Tender.tenderlinenumber;

                                    // Insert Payments
                                    dataContext.PaymentsHDRs.InsertOnSubmit(PayHDR);
                                    dataContext.SubmitChanges();

                                    dataContext.PaymentsLINEs.InsertOnSubmit(PayLine);
                                    dataContext.SubmitChanges();
                                }
                            }
                        }
                    }

                    InvHDR.ID = BASETRANID;
                    InvHDR.CardCode = CustCode;
                    InvHDR.NumAtCard = SHDetail.invoicenumber;
                    InvHDR.OrderID = SHDetail.invoicenumber;
                    InvHDR.OrderTotal = SHDetail.totalgstinclusive.ToString();
                    InvHDR.OrderTax = SHDetail.gst.ToString();
                    InvHDR.OrderCurrency = Currency;
                    InvHDR.CreateDate = DateTime.ParseExact(SHDetail.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                    InvHDR.DocDate = DateTime.ParseExact(SHDetail.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                    InvHDR.DocDueDate = DateTime.ParseExact(SHDetail.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                    InvHDR.U_MagBatchID = BatchID;
                    InvHDR.U_MagTranID = TranID;
                    InvHDR.U_MagTranNumber = SHDetail.transactionnumber;
                    InvHDR.U_MagInvoiceNumber = SHDetail.invoicenumber;
                    InvHDR.U_MagOrderType = SHType;
                    InvHDR.U_MagCustomerCode = SHDetail.customernumber;
                    InvHDR.U_MagHosFileRef = SHDetail.numberofhosfilerows;
                    InvHDR.U_MagTNChangeCount = changecountrec;
                    InvHDR.Status = "Queued";
                    InvHDR.StatusDate = DateTime.Now;
                    InvHDR.StatusMessage = "";

                    // Insert Invoice Header
                    dataContext.ARInvoicesHDRs.InsertOnSubmit(InvHDR);
                    dataContext.SubmitChanges();

                    break;
                case "CREDIT":
                    WHID = dataContext.fn_SAPWHCode(SHDetail.branchcode).ToString();
                    CustCode = dataContext.fn_SAPCardCode(SHDetail.customernumber).ToString();

                    foreach (Sl Line in SHDetail.sl)
                    {
                        ARCreditMemosLINE CreditLine = new ARCreditMemosLINE();
                        CreditLine.OrderID = SHDetail.invoicenumber;
                        CreditLine.OriginalOrderID = Line.originalinvoicenumber;
                        CreditLine.ItemCode = Line.plu;
                        CreditLine.Quantity = Convert.ToDecimal((Line.quantity * -1));
                        CreditLine.HDRID = BASETRANID;
                        CreditLine.UnitPrice = Convert.ToDecimal(Line.unitpriceinclgst);

                        if (Line.gstrate == 0)
                        {
                            CreditLine.TaxCode = TaxFreecode;
                        }
                        else
                        {
                            CreditLine.TaxCode = Taxcode;
                        }

                        CreditLine.WarehouseCode = WHID;
                        CreditLine.U_MagInvoiceNumber = Line.invoicenumber;
                        CreditLine.U_MagLineNumber = Line.linenumber;
                        CreditLine.U_MagReturnCode = Line.returnreasoncode;
                        CreditLine.U_MagDiscountReasonCode = Line.discountreasoncode;
                        CreditLine.U_MagDiscountSubReasonCode = Line.discountsubreasoncode;
                        CreditLine.U_MagPromoCode = Line.promotioncode;
                        CreditLine.U_MagTranID = TranID;
                        if (SHDetail.Sixty != null)
                        {
                            CreditLine.U_MagOrigInvNumber = SHDetail.Sixty.Where(x => x.linenumber == Line.linenumber).FirstOrDefault() == null ? "" : SHDetail.Sixty.Where(x => x.linenumber == Line.linenumber).FirstOrDefault().originalinvoicenumber;
                            CreditLine.U_MagOrigLineNumber = CreditLine.U_MagOrigLineNumber = SHDetail.Sixty.Where(x => x.linenumber == Line.linenumber).FirstOrDefault() == null ? -1 : SHDetail.Sixty.Where(x => x.linenumber == Line.linenumber).FirstOrDefault().originalsaleline;
                            CreditLine.U_MagReturnDetail = SHDetail.Sixty.Where(x => x.linenumber == Line.linenumber).FirstOrDefault() == null ? "" : SHDetail.Sixty.Where(x => x.linenumber == Line.linenumber).FirstOrDefault().returnreasonextradetail;
                        }

                        // Insert Credit Lines
                        dataContext.ARCreditMemosLINEs.InsertOnSubmit(CreditLine);
                        dataContext.SubmitChanges();
                    }

                    PAYTRANIDNULL = dataContext.fn_TranID("PAYMENT");
                    PAYTRANID = PAYTRANIDNULL ?? 0;
                    vouchertotal = 0;
                    changecountrec = 0;
                    changeapportioned = 0;

                    if (SHDetail.oc != null)
                    {
                        foreach (Oc Voucher in SHDetail.oc)
                        {
                            ARCreditMemosLINE VoucherCRLine = new ARCreditMemosLINE();
                            VoucherCRLine.OrderID = SHDetail.invoicenumber;
                            VoucherCRLine.ItemCode = "VOUCHER";
                            VoucherCRLine.Quantity = -1;
                            VoucherCRLine.HDRID = BASETRANID;
                            VoucherCRLine.TaxCode = TaxFreecode;
                            VoucherCRLine.WarehouseCode = WHID;
                            VoucherCRLine.U_MagInvoiceNumber = SHDetail.invoicenumber;
                            VoucherCRLine.U_MagVoucherCode = Voucher.vouchernumber;
                            VoucherCRLine.U_MagTranID = TranID;

                            if (Voucher.issueamount < 0)
                            {
                                VoucherCRLine.U_MagVoucherAmt = Convert.ToDecimal(Voucher.issueamount * -1);
                                VoucherCRLine.UnitPrice = Convert.ToDecimal(Voucher.issueamount * -1);
                            }
                            else
                            {
                                VoucherCRLine.U_MagVoucherAmt = Convert.ToDecimal(Voucher.issueamount);
                                VoucherCRLine.UnitPrice = Convert.ToDecimal(Voucher.issueamount);
                            }

                            // Insert Invoice Lines
                            dataContext.ARCreditMemosLINEs.InsertOnSubmit(VoucherCRLine);
                            dataContext.SubmitChanges();

                            vouchertotal = vouchertotal + Convert.ToDecimal(Voucher.issueamount);
                        }
                    }

                    if (SHDetail.tn != null)
                    {
                        foreach (Tn Tender in SHDetail.tn)
                        {
                            if (Tender.changerecords == true)
                                changecountrec++;
                        }

                        foreach (Tn Tender in SHDetail.tn)
                        {
                            if (Tender.changerecords == false && Tender.tendercode != "ACCOUNT")
                            {
                                TranAccount = dataContext.fn_PaymentAccount(Tender.tendercode, WHID);
                                PAYTRANID++;
                                PaymentsHDR PayHDR = new PaymentsHDR();
                                PaymentsLINE PayLine = new PaymentsLINE();
                                PayHDR.ID = PAYTRANID;
                                PayHDR.CardCode = CustCode;
                                PayHDR.CreateDate = DateTime.ParseExact(Tender.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                                PayHDR.DocDate = DateTime.ParseExact(Tender.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                                PayHDR.TransferDate = DateTime.ParseExact(Tender.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                                PayHDR.TransferAccount = TranAccount;
                                PayHDR.OrderID = SHDetail.invoicenumber;
                                PayHDR.TransferSum = Convert.ToDecimal((Tender.amount - Tender.changeapportionedtothistender) * -1);
                                PayHDR.U_MagBatchID = BatchID;
                                PayHDR.U_MagTranID = TranID;
                                PayHDR.U_MagTranNumber = Tender.transactionnumber;
                                PayHDR.U_MagInvoiceNumber = Tender.invoicenumber;
                                PayHDR.U_MagPaymentMethod = Tender.tendercode;
                                PayHDR.U_MagCustomerCode = SHDetail.customernumber;
                                PayHDR.U_MagHosFileRef = SHDetail.numberofhosfilerows;
                                PayHDR.U_MagTNChangeCount = changecountrec;
                                PayHDR.U_MagLineNumber = Tender.tenderlinenumber;
                                PayHDR.IN_OUT = "OUT";
                                PayHDR.Status = "Queued";
                                PayHDR.StatusDate = DateTime.Now;
                                PayHDR.StatusMessage = "";

                                if (Tender.tendercode == "GC")
                                    PayHDR.U_MagVoucherCode = Tender.refnumber;

                                PayLine.HDRID = PAYTRANID;
                                PayLine.OrderID = SHDetail.invoicenumber;
                                PayLine.InvoiceType = "it_CredItnote";
                                PayLine.SumApplied = Convert.ToDecimal((Tender.amount - Tender.changeapportionedtothistender) * -1);
                                PayLine.U_MagTranID = TranID;
                                PayLine.U_MagLineNumber = Tender.tenderlinenumber;

                                // Insert Payments
                                dataContext.PaymentsHDRs.InsertOnSubmit(PayHDR);
                                dataContext.SubmitChanges();

                                dataContext.PaymentsLINEs.InsertOnSubmit(PayLine);
                                dataContext.SubmitChanges();
                            }
                        }
                    }

                    CreditHDR.ID = BASETRANID;
                    CreditHDR.CardCode = CustCode;
                    CreditHDR.NumAtCard = SHDetail.invoicenumber;
                    CreditHDR.OrderID = SHDetail.invoicenumber;
                    CreditHDR.OrderTotal = (SHDetail.totalgstinclusive * -1).ToString();
                    CreditHDR.OrderTax = (SHDetail.gst * -1).ToString();
                    CreditHDR.OrderCurrency = Currency;
                    CreditHDR.CreateDate = DateTime.ParseExact(SHDetail.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                    CreditHDR.DocDate = DateTime.ParseExact(SHDetail.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                    CreditHDR.DocDueDate = DateTime.ParseExact(SHDetail.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                    CreditHDR.U_MagBatchID = BatchID;
                    CreditHDR.U_MagTranID = TranID;
                    CreditHDR.U_MagTranNumber = SHDetail.transactionnumber;
                    CreditHDR.U_MagInvoiceNumber = SHDetail.invoicenumber;
                    CreditHDR.U_MagOrderType = SHType;
                    CreditHDR.U_MagCustomerCode = SHDetail.customernumber;
                    CreditHDR.U_MagHosFileRef = SHDetail.numberofhosfilerows;
                    CreditHDR.U_MagTNChangeCount = changecountrec;
                    CreditHDR.Status = "Queued";
                    CreditHDR.StatusDate = DateTime.Now;
                    CreditHDR.StatusMessage = "";

                    // Insert Credit Header
                    dataContext.ARCreditMemosHDRs.InsertOnSubmit(CreditHDR);
                    dataContext.SubmitChanges();

                    break;
                case "LAYBY":
                    WHID = dataContext.fn_SAPWHCode(SHDetail.branchcode).ToString();
                    CustCode = dataContext.fn_SAPCardCode(SHDetail.customernumber).ToString();
                    OrderSubType = "";
                    OrderSubTypeLine1 = true;

                    foreach (Ot Line in SHDetail.ot)
                    {
                        ARPreOrdersLINE OrdLine = new ARPreOrdersLINE();
                        OrdLine.OrderID = SHDetail.invoicenumber;
                        OrdLine.OriginalOrderID = Line.originalinvoicenumber;
                        OrdLine.ItemCode = Line.plu;
                        OrdLine.Quantity = Convert.ToDecimal(Line.quantity);
                        OrdLine.HDRID = BASETRANID;
                        OrdLine.UnitPrice = Convert.ToDecimal(Line.unitpriceinclgst);

                        if (Line.gstrate == 0)
                        {
                            OrdLine.TaxCode = TaxFreecode;
                        }
                        else
                        {
                            OrdLine.TaxCode = Taxcode;
                        }

                        OrdLine.WarehouseCode = WHID;
                        OrdLine.U_MagOrderNumber = Line.ordernumber;
                        OrdLine.U_MagInvoiceNumber = Line.invoicenumber;
                        OrdLine.U_MagLineNumber = Line.linenumber;
                        OrdLine.U_MagPromoCode = ""; // Line.promotioncode;
                        OrdLine.U_MagTranID = TranID;
                        if (SHDetail.Sixty != null)
                        {
                            OrdLine.U_MagOrigInvNumber = SHDetail.Sixty.Where(x => x.linenumber == Line.linenumber).FirstOrDefault() == null ? "" : SHDetail.Sixty.Where(x => x.linenumber == Line.linenumber).FirstOrDefault().originalinvoicenumber;
                            OrdLine.U_MagOrigLineNumber = SHDetail.Sixty.Where(x => x.linenumber == Line.linenumber).FirstOrDefault() == null ? -1 : SHDetail.Sixty.Where(x => x.linenumber == Line.linenumber).FirstOrDefault().originalsaleline;
                        }

                        if (OrderSubTypeLine1)
                        {
                            OrderSubType = Line.linestatus;
                            OrderSubTypeLine1 = false;
                        }

                        // Insert Order Lines
                        dataContext.ARPreOrdersLINEs.InsertOnSubmit(OrdLine);
                        dataContext.SubmitChanges();
                    }

                    if (SHDetail.oc != null)
                    {
                        foreach (Oc Voucher in SHDetail.oc)
                        {
                            ARPreOrdersLINE VoucherORDLine = new ARPreOrdersLINE();
                            VoucherORDLine.OrderID = SHDetail.invoicenumber;
                            VoucherORDLine.ItemCode = "VOUCHER";
                            VoucherORDLine.Quantity = 1;
                            VoucherORDLine.HDRID = BASETRANID;
                            VoucherORDLine.TaxCode = TaxFreecode;
                            VoucherORDLine.WarehouseCode = WHID;
                            VoucherORDLine.U_MagInvoiceNumber = SHDetail.invoicenumber;
                            VoucherORDLine.U_MagVoucherCode = Voucher.vouchernumber;
                            VoucherORDLine.U_MagTranID = TranID;

                            if (Voucher.issueamount < 0)
                            {
                                VoucherORDLine.U_MagVoucherAmt = Convert.ToDecimal(Voucher.issueamount * -1);
                                VoucherORDLine.UnitPrice = Convert.ToDecimal(Voucher.issueamount * -1);
                            }
                            else
                            {
                                VoucherORDLine.U_MagVoucherAmt = Convert.ToDecimal(Voucher.issueamount);
                                VoucherORDLine.UnitPrice = Convert.ToDecimal(Voucher.issueamount);
                            }

                            // Insert Invoice Lines
                            dataContext.ARPreOrdersLINEs.InsertOnSubmit(VoucherORDLine);
                            dataContext.SubmitChanges();

                            vouchertotal = vouchertotal + Convert.ToDecimal(Voucher.issueamount);
                        }
                    }

                    PAYTRANIDNULL = dataContext.fn_TranID("PAYMENT");
                    PAYTRANID = PAYTRANIDNULL ?? 0;
                    changecountrec = 0;
                    changeapportioned = 0;

                    if (SHDetail.tn != null)
                    {
                        foreach (Tn Tender in SHDetail.tn)
                        {
                            if (Tender.changerecords == true)
                                changecountrec++;
                        }

                        foreach (Tn Tender in SHDetail.tn)
                        {
                            if (Tender.changerecords == false)
                            {
                                decimal tenderamount = Convert.ToDecimal(Tender.amount);
                                decimal change = Convert.ToDecimal(Tender.changeapportionedtothistender);
                                string INOUTVAL = "";

                                if (Tender.tendercode == "CASH")
                                {
                                    if (changeapportioned <= change)
                                    {
                                        if (tenderamount > (change - changeapportioned))
                                        {
                                            decimal tranchange = (change - changeapportioned);
                                            tenderamount = Convert.ToDecimal(tenderamount - tranchange);
                                            changeapportioned = (changeapportioned + tranchange);
                                        }
                                        else
                                        {
                                            if (OrderSubType != "C")
                                            {
                                                changeapportioned = (changeapportioned + tenderamount);
                                                continue;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        tenderamount = Convert.ToDecimal(Tender.amount);
                                    }
                                }
                                else
                                {
                                    tenderamount = Convert.ToDecimal(Tender.amount);
                                }

                                if (OrderSubType == "C" && Tender.tendercode == "GC")
                                {
                                    INOUTVAL = "CNL";
                                    tenderamount = Convert.ToDecimal(tenderamount * -1);
                                }
                                else if (OrderSubType == "C" && Tender.tendercode != "GC")
                                {
                                    INOUTVAL = "OUT";
                                    tenderamount = Convert.ToDecimal(tenderamount * -1);
                                }
                                else
                                {
                                    INOUTVAL = "IN";
                                    tenderamount = Convert.ToDecimal(tenderamount);
                                }

                                if (tenderamount > 0 && Tender.tendercode != "ACCOUNT")
                                {
                                    TranAccount = dataContext.fn_PaymentAccount(Tender.tendercode, WHID);
                                    PAYTRANID++;
                                    PaymentsHDR PayHDR = new PaymentsHDR();
                                    PaymentsLINE PayLine = new PaymentsLINE();
                                    PayHDR.ID = PAYTRANID;
                                    PayHDR.CardCode = CustCode;
                                    PayHDR.CreateDate = DateTime.ParseExact(Tender.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                                    PayHDR.DocDate = DateTime.ParseExact(Tender.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                                    PayHDR.TransferDate = DateTime.ParseExact(Tender.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                                    PayHDR.TransferAccount = TranAccount;
                                    PayHDR.OrderID = SHDetail.oh.ordernumber;
                                    PayHDR.U_MagBatchID = BatchID;
                                    PayHDR.U_MagTranID = TranID;
                                    PayHDR.U_MagTranNumber = Tender.transactionnumber;
                                    PayHDR.U_MagInvoiceNumber = Tender.invoicenumber;
                                    PayHDR.U_MagOrderNumber = SHDetail.oh.ordernumber;
                                    PayHDR.U_MagPaymentMethod = Tender.tendercode;
                                    PayHDR.U_MagCustomerCode = SHDetail.customernumber;
                                    PayHDR.U_MagHosFileRef = SHDetail.numberofhosfilerows;
                                    PayHDR.U_MagTNChangeCount = changecountrec;
                                    PayHDR.U_MagLineNumber = Tender.tenderlinenumber;

                                    PayHDR.IN_OUT = INOUTVAL;
                                    PayHDR.TransferSum = tenderamount;

                                    PayHDR.Status = "Queued";
                                    PayHDR.StatusDate = DateTime.Now;
                                    PayHDR.StatusMessage = "";

                                    if (Tender.tendercode == "GC")
                                        PayHDR.U_MagVoucherCode = Tender.refnumber;

                                    PayLine.HDRID = PAYTRANID;
                                    PayLine.OrderID = SHDetail.invoicenumber;
                                    PayLine.InvoiceType = "it_Order";
                                    PayLine.SumApplied = Convert.ToDecimal(tenderamount * -1);
                                    PayLine.U_MagTranID = TranID;
                                    PayLine.U_MagLineNumber = Tender.tenderlinenumber;

                                    // Insert Payments
                                    dataContext.PaymentsHDRs.InsertOnSubmit(PayHDR);
                                    dataContext.SubmitChanges();

                                    dataContext.PaymentsLINEs.InsertOnSubmit(PayLine);
                                    dataContext.SubmitChanges();
                                }
                            }
                        }
                    }

                    OrdHDR.ID = BASETRANID;
                    OrdHDR.CardCode = CustCode;
                    OrdHDR.NumAtCard = SHDetail.invoicenumber;
                    OrdHDR.OrderID = SHDetail.oh.ordernumber;
                    OrdHDR.OrderTotal = SHDetail.totalgstinclusive.ToString();
                    OrdHDR.OrderTax = SHDetail.gst.ToString();
                    OrdHDR.OrderCurrency = Currency;
                    OrdHDR.CreateDate = DateTime.ParseExact(SHDetail.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                    OrdHDR.DocDate = DateTime.ParseExact(SHDetail.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                    OrdHDR.DocDueDate = DateTime.ParseExact(SHDetail.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                    OrdHDR.U_MagBatchID = BatchID;
                    OrdHDR.U_MagTranID = TranID;
                    OrdHDR.U_MagTranNumber = SHDetail.transactionnumber;
                    OrdHDR.U_MagInvoiceNumber = SHDetail.invoicenumber;
                    OrdHDR.U_MagOrderNumber = SHDetail.oh.ordernumber;
                    OrdHDR.U_MagOrderType = SHType;
                    OrdHDR.U_MagCustomerCode = SHDetail.customernumber;
                    OrdHDR.U_MagHosFileRef = SHDetail.numberofhosfilerows;
                    OrdHDR.U_MagTNChangeCount = changecountrec;
                    OrdHDR.U_MagOrderSubType = OrderSubType;
                    OrdHDR.Status = "Queued";
                    OrdHDR.StatusDate = DateTime.Now;
                    OrdHDR.StatusMessage = "";

                    // Insert Invoice Header
                    dataContext.ARPreOrdersHDRs.InsertOnSubmit(OrdHDR);
                    dataContext.SubmitChanges();
                    break;
                case "LAYBYINVOICE":
                    WHID = dataContext.fn_SAPWHCode(SHDetail.branchcode).ToString();
                    CustCode = dataContext.fn_SAPCardCode(SHDetail.customernumber).ToString();

                    foreach (Sl Line in SHDetail.sl)
                    {
                        ARInvoicesLINE InvLine = new ARInvoicesLINE();
                        InvLine.OrderID = SHDetail.invoicenumber;
                        InvLine.OriginalOrderID = Line.originalinvoicenumber;
                        InvLine.ItemCode = Line.plu;
                        InvLine.Quantity = Convert.ToDecimal(Line.quantity);
                        InvLine.HDRID = BASETRANID;
                        InvLine.UnitPrice = Convert.ToDecimal(Line.unitpriceinclgst);

                        if (Line.gstrate == 0)
                        {
                            InvLine.TaxCode = TaxFreecode;
                        }
                        else
                        {
                            InvLine.TaxCode = Taxcode;
                        }

                        InvLine.WarehouseCode = WHID;
                        InvLine.U_MagInvoiceNumber = Line.invoicenumber;
                        InvLine.U_MagLineNumber = Line.linenumber;
                        InvLine.U_MagPromoCode = Line.promotioncode;
                        InvLine.U_MagTranID = TranID;
                        if (SHDetail.ot != null)
                        {
                            InvLine.U_MagOrigInvNumber = SHDetail.ot.Where(x => x.linenumber == Line.linenumber).FirstOrDefault() == null ? "" : SHDetail.ot.Where(x => x.linenumber == Line.linenumber).FirstOrDefault().ordernumber;
                            InvLine.U_MagOrigLineNumber = SHDetail.ot.Where(x => x.linenumber == Line.linenumber).FirstOrDefault() == null ? -1 : SHDetail.ot.Where(x => x.linenumber == Line.linenumber).FirstOrDefault().linenumber;
                        }

                        // Insert Invoice Lines
                        dataContext.ARInvoicesLINEs.InsertOnSubmit(InvLine);
                        dataContext.SubmitChanges();
                    }

                    PAYTRANIDNULL = dataContext.fn_TranID("PAYMENT");
                    PAYTRANID = PAYTRANIDNULL ?? 0;
                    changecountrec = 0;
                    changeapportioned = 0;

                    if (SHDetail.tn != null)
                    {
                        foreach (Tn Tender in SHDetail.tn)
                        {
                            if (Tender.changerecords == true)
                                changecountrec++;
                        }

                        foreach (Tn Tender in SHDetail.tn)
                        {
                            if (Tender.changerecords == false)
                            {
                                decimal tenderamount = Convert.ToDecimal(Tender.amount);
                                decimal change = Convert.ToDecimal(Tender.changeapportionedtothistender);

                                if (Tender.tendercode == "CASH")
                                {
                                    if (changeapportioned <= change)
                                    {
                                        if (tenderamount > (change - changeapportioned))
                                        {
                                            decimal tranchange = (change - changeapportioned);
                                            tenderamount = Convert.ToDecimal(tenderamount - tranchange);
                                            changeapportioned = (changeapportioned + tranchange);
                                        }
                                        else
                                        {
                                            changeapportioned = (changeapportioned + tenderamount);
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        tenderamount = Convert.ToDecimal(Tender.amount);
                                    }
                                }
                                else
                                {
                                    tenderamount = Convert.ToDecimal(Tender.amount);
                                }

                                if (tenderamount > 0 && Tender.tendercode != "ACCOUNT")
                                {
                                    TranAccount = dataContext.fn_PaymentAccount(Tender.tendercode, WHID);
                                    PAYTRANID++;
                                    PaymentsHDR PayHDR = new PaymentsHDR();
                                    PaymentsLINE PayLine = new PaymentsLINE();
                                    PayHDR.ID = PAYTRANID;
                                    PayHDR.CardCode = CustCode;
                                    PayHDR.CreateDate = DateTime.ParseExact(Tender.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                                    PayHDR.DocDate = DateTime.ParseExact(Tender.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                                    PayHDR.TransferDate = DateTime.ParseExact(Tender.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                                    PayHDR.TransferAccount = TranAccount;
                                    PayHDR.OrderID = SHDetail.oh.ordernumber;
                                    PayHDR.TransferSum = tenderamount;
                                    PayHDR.U_MagBatchID = BatchID;
                                    PayHDR.U_MagTranID = TranID;
                                    PayHDR.U_MagTranNumber = Tender.transactionnumber;
                                    PayHDR.U_MagInvoiceNumber = Tender.invoicenumber;
                                    PayHDR.U_MagOrderNumber = SHDetail.oh.ordernumber;
                                    PayHDR.U_MagPaymentMethod = Tender.tendercode;
                                    PayHDR.U_MagCustomerCode = SHDetail.customernumber;
                                    PayHDR.U_MagHosFileRef = SHDetail.numberofhosfilerows;
                                    PayHDR.U_MagTNChangeCount = changecountrec;
                                    PayHDR.U_MagLineNumber = Tender.tenderlinenumber;
                                    PayHDR.IN_OUT = "IN";
                                    PayHDR.Status = "Queued";
                                    PayHDR.StatusDate = DateTime.Now;
                                    PayHDR.StatusMessage = "";

                                    if (Tender.tendercode == "GC")
                                        PayHDR.U_MagVoucherCode = Tender.refnumber;

                                    PayLine.HDRID = PAYTRANID;
                                    PayLine.OrderID = SHDetail.invoicenumber;
                                    PayLine.InvoiceType = "it_Order";
                                    PayLine.SumApplied = Convert.ToDecimal(tenderamount * -1);
                                    PayLine.U_MagTranID = TranID;
                                    PayLine.U_MagLineNumber = Tender.tenderlinenumber;

                                    // Insert Payments
                                    dataContext.PaymentsHDRs.InsertOnSubmit(PayHDR);
                                    dataContext.SubmitChanges();

                                    dataContext.PaymentsLINEs.InsertOnSubmit(PayLine);
                                    dataContext.SubmitChanges();
                                }
                            }
                        }
                    }

                    InvHDR.ID = BASETRANID;
                    InvHDR.CardCode = CustCode;
                    InvHDR.NumAtCard = SHDetail.invoicenumber;
                    InvHDR.OrderID = SHDetail.invoicenumber;
                    InvHDR.OrderTotal = SHDetail.totalgstinclusive.ToString();
                    InvHDR.OrderTax = SHDetail.gst.ToString();
                    InvHDR.OrderCurrency = Currency;
                    InvHDR.CreateDate = DateTime.ParseExact(SHDetail.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                    InvHDR.DocDate = DateTime.ParseExact(SHDetail.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                    InvHDR.DocDueDate = DateTime.ParseExact(SHDetail.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                    InvHDR.U_MagBatchID = BatchID;
                    InvHDR.U_MagTranID = TranID;
                    InvHDR.U_MagTranNumber = SHDetail.transactionnumber;
                    InvHDR.U_MagInvoiceNumber = SHDetail.invoicenumber;
                    InvHDR.U_MagOrderNumber = SHDetail.oh.ordernumber;
                    InvHDR.U_MagOrderType = SHType;
                    InvHDR.U_MagCustomerCode = SHDetail.customernumber;
                    InvHDR.U_MagHosFileRef = SHDetail.numberofhosfilerows;
                    InvHDR.U_MagTNChangeCount = changecountrec;
                    InvHDR.Status = "Queued";
                    InvHDR.StatusDate = DateTime.Now;
                    InvHDR.StatusMessage = "";

                    // Insert Invoice Header
                    dataContext.ARInvoicesHDRs.InsertOnSubmit(InvHDR);
                    dataContext.SubmitChanges();
                    break;
                case "PAYMENTONLY":
                    WHID = dataContext.fn_SAPWHCode(SHDetail.branchcode).ToString();
                    CustCode = dataContext.fn_SAPCardCode(SHDetail.customernumber).ToString();

                    PAYTRANIDNULL = dataContext.fn_TranID("PAYMENTONLY");
                    PAYTRANID = PAYTRANIDNULL ?? 0;
                    changecountrec = 0;
                    changeapportioned = 0;

                    foreach (Tn Tender in SHDetail.tn)
                    {
                        if (Tender.changerecords == true)
                            changecountrec++;
                    }

                    foreach (Tn Tender in SHDetail.tn)
                    {
                        if (Tender.changerecords == false)
                        {
                            decimal tenderamount = Convert.ToDecimal(Tender.amount);
                            decimal change = Convert.ToDecimal(Tender.changeapportionedtothistender);
                            string INOUTVAL = "";

                            if (Tender.tendercode == "CASH")
                            {
                                if (changeapportioned <= change)
                                {
                                    if (tenderamount > (change - changeapportioned))
                                    {
                                        decimal tranchange = (change - changeapportioned);
                                        tenderamount = Convert.ToDecimal(tenderamount - tranchange);
                                        changeapportioned = (changeapportioned + tranchange);
                                    }
                                    else
                                    {
                                        changeapportioned = (changeapportioned + tenderamount);
                                        continue;
                                    }
                                }
                                else
                                {
                                    tenderamount = Convert.ToDecimal(Tender.amount);
                                }
                            }
                            else
                            {
                                tenderamount = Convert.ToDecimal(Tender.amount);
                            }

                            if ((Tender.amount - Tender.changeapportionedtothistender) < 0)
                            {
                                tenderamount = Convert.ToDecimal(tenderamount * -1);
                                INOUTVAL = "OUT";
                            }
                            else
                            {
                                tenderamount = Convert.ToDecimal(tenderamount);
                                INOUTVAL = "IN";
                            }

                            if (tenderamount > 0 && Tender.tendercode != "ACCOUNT")
                            {
                                TranAccount = dataContext.fn_PaymentAccount(Tender.tendercode, WHID);
                                PAYTRANID++;
                                PaymentsHDR PayHDR = new PaymentsHDR();
                                PaymentsLINE PayLine = new PaymentsLINE();
                                PayHDR.ID = PAYTRANID;
                                PayHDR.CardCode = CustCode;
                                PayHDR.CreateDate = DateTime.ParseExact(Tender.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                                PayHDR.DocDate = DateTime.ParseExact(Tender.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                                PayHDR.TransferDate = DateTime.ParseExact(Tender.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                                PayHDR.TransferAccount = TranAccount;
                                PayHDR.OrderID = SHDetail.op[0].ordernumber;

                                PayHDR.TransferSum = tenderamount;
                                PayHDR.IN_OUT = INOUTVAL;

                                PayHDR.U_MagBatchID = BatchID;
                                PayHDR.U_MagTranID = TranID;
                                PayHDR.U_MagTranNumber = Tender.transactionnumber;
                                PayHDR.U_MagInvoiceNumber = Tender.invoicenumber;
                                PayHDR.U_MagOrderNumber = SHDetail.op[0].ordernumber;
                                PayHDR.U_MagPaymentMethod = Tender.tendercode;
                                PayHDR.U_MagCustomerCode = SHDetail.customernumber;
                                PayHDR.U_MagHosFileRef = SHDetail.numberofhosfilerows;
                                PayHDR.U_MagTNChangeCount = changecountrec;
                                PayHDR.U_MagLineNumber = Tender.tenderlinenumber;
                                PayHDR.Status = "Queued";
                                PayHDR.StatusDate = DateTime.Now;
                                PayHDR.StatusMessage = "";

                                if (Tender.tendercode == "GC")
                                    PayHDR.U_MagVoucherCode = Tender.refnumber;

                                // Insert Payments
                                dataContext.PaymentsHDRs.InsertOnSubmit(PayHDR);
                                dataContext.SubmitChanges();
                            }
                        }
                    }
                    break;
                case "GIFTCARDONLY":
                    WHID = dataContext.fn_SAPWHCode(SHDetail.branchcode).ToString();
                    CustCode = dataContext.fn_SAPCardCode(SHDetail.customernumber).ToString();
                    decimal tenderamounttotal = 0;

                    if (SHDetail.tn != null)
                    {
                        PAYTRANIDNULL = dataContext.fn_TranID("PAYMENT");
                        PAYTRANID = PAYTRANIDNULL ?? 0;
                        vouchertotal = 0;
                        changecountrec = 0;
                        changeapportioned = 0;

                        if (SHDetail.oc != null)
                        {
                            foreach (Oc Voucher in SHDetail.oc)
                            {
                                ARInvoicesLINE VoucherInvLine = new ARInvoicesLINE();
                                VoucherInvLine.OrderID = SHDetail.invoicenumber;
                                VoucherInvLine.ItemCode = "VOUCHER";
                                VoucherInvLine.Quantity = 1;
                                VoucherInvLine.HDRID = BASETRANID;
                                VoucherInvLine.UnitPrice = Convert.ToDecimal(Voucher.issueamount);
                                VoucherInvLine.TaxCode = TaxFreecode;
                                VoucherInvLine.WarehouseCode = WHID;
                                VoucherInvLine.U_MagInvoiceNumber = SHDetail.invoicenumber;
                                VoucherInvLine.U_MagVoucherCode = Voucher.vouchernumber;
                                VoucherInvLine.U_MagTranID = TranID;

                                if (Voucher.issueamount < 0)
                                {
                                    VoucherInvLine.U_MagVoucherAmt = Convert.ToDecimal(Voucher.issueamount * -1);
                                    VoucherInvLine.UnitPrice = Convert.ToDecimal(Voucher.issueamount * -1);
                                }
                                else
                                {
                                    VoucherInvLine.U_MagVoucherAmt = Convert.ToDecimal(Voucher.issueamount);
                                    VoucherInvLine.UnitPrice = Convert.ToDecimal(Voucher.issueamount);
                                }

                                // Insert Invoice Lines
                                dataContext.ARInvoicesLINEs.InsertOnSubmit(VoucherInvLine);
                                dataContext.SubmitChanges();
                            }
                        }

                        foreach (Tn Tender in SHDetail.tn)
                        {
                            if (Tender.changerecords == true)
                                changecountrec++;
                        }


                        foreach (Tn Tender in SHDetail.tn)
                        {
                            if (Tender.changerecords == false)
                            {
                                decimal tenderamount = Convert.ToDecimal(Tender.amount);
                                decimal change = Convert.ToDecimal(Tender.changeapportionedtothistender);

                                if (Tender.tendercode == "CASH")
                                {
                                    if (changeapportioned <= change)
                                    {
                                        if (tenderamount > (change - changeapportioned))
                                        {
                                            decimal tranchange = (change - changeapportioned);
                                            tenderamount = Convert.ToDecimal(tenderamount - tranchange);
                                            changeapportioned = (changeapportioned + tranchange);
                                        }
                                        else
                                        {
                                            changeapportioned = (changeapportioned + tenderamount);
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        tenderamount = Convert.ToDecimal(Tender.amount);
                                    }
                                }
                                else
                                {
                                    tenderamount = Convert.ToDecimal(Tender.amount);
                                }

                                if (tenderamount > 0 && Tender.tendercode != "ACCOUNT")
                                {
                                    TranAccount = dataContext.fn_PaymentAccount(Tender.tendercode, WHID);
                                    PAYTRANID++;
                                    PaymentsHDR PayHDR = new PaymentsHDR();
                                    PaymentsLINE PayLine = new PaymentsLINE();
                                    PayHDR.ID = PAYTRANID;
                                    PayHDR.CardCode = CustCode;
                                    PayHDR.CreateDate = DateTime.ParseExact(Tender.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                                    PayHDR.DocDate = DateTime.ParseExact(Tender.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                                    PayHDR.TransferDate = DateTime.ParseExact(Tender.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                                    PayHDR.TransferAccount = TranAccount;
                                    PayHDR.OrderID = SHDetail.invoicenumber;
                                    PayHDR.TransferSum = tenderamount;
                                    PayHDR.U_MagBatchID = BatchID;
                                    PayHDR.U_MagTranID = TranID;
                                    PayHDR.U_MagTranNumber = Tender.transactionnumber;
                                    PayHDR.U_MagInvoiceNumber = Tender.invoicenumber;
                                    PayHDR.U_MagPaymentMethod = Tender.tendercode;
                                    PayHDR.U_MagCustomerCode = SHDetail.customernumber;
                                    PayHDR.U_MagHosFileRef = SHDetail.numberofhosfilerows;
                                    PayHDR.U_MagTNChangeCount = changecountrec;
                                    PayHDR.U_MagLineNumber = Tender.tenderlinenumber;
                                    PayHDR.IN_OUT = "IN";
                                    PayHDR.Status = "Queued";
                                    PayHDR.StatusDate = DateTime.Now;
                                    PayHDR.StatusMessage = "";

                                    if (Tender.tendercode == "GC")
                                        PayHDR.U_MagVoucherCode = Tender.refnumber;

                                    PayLine.HDRID = PAYTRANID;
                                    PayLine.OrderID = SHDetail.invoicenumber;
                                    PayLine.InvoiceType = "it_Invoice";
                                    PayLine.SumApplied = tenderamount;
                                    PayLine.U_MagTranID = TranID;
                                    PayLine.U_MagLineNumber = Tender.tenderlinenumber;

                                    tenderamounttotal = tenderamounttotal + tenderamount;

                                    // Insert Payments
                                    dataContext.PaymentsHDRs.InsertOnSubmit(PayHDR);
                                    dataContext.SubmitChanges();

                                    dataContext.PaymentsLINEs.InsertOnSubmit(PayLine);
                                    dataContext.SubmitChanges();
                                }
                            }
                        }
                    }

                    InvHDR.ID = BASETRANID;
                    InvHDR.CardCode = CustCode;
                    InvHDR.NumAtCard = SHDetail.invoicenumber;
                    InvHDR.OrderID = SHDetail.invoicenumber;
                    InvHDR.OrderTotal = tenderamounttotal.ToString();
                    InvHDR.OrderTax = "0";
                    InvHDR.OrderCurrency = Currency;
                    InvHDR.CreateDate = DateTime.ParseExact(SHDetail.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                    InvHDR.DocDate = DateTime.ParseExact(SHDetail.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                    InvHDR.DocDueDate = DateTime.ParseExact(SHDetail.date.Trim(), "yyyyMMdd", CultureInfo.InvariantCulture);
                    InvHDR.U_MagBatchID = BatchID;
                    InvHDR.U_MagTranID = TranID;
                    InvHDR.U_MagTranNumber = SHDetail.transactionnumber;
                    InvHDR.U_MagInvoiceNumber = SHDetail.invoicenumber;
                    InvHDR.U_MagOrderType = SHType;
                    InvHDR.U_MagCustomerCode = SHDetail.customernumber;
                    InvHDR.U_MagHosFileRef = SHDetail.numberofhosfilerows;
                    InvHDR.U_MagTNChangeCount = changecountrec;
                    InvHDR.Status = "Queued";
                    InvHDR.StatusDate = DateTime.Now;
                    InvHDR.StatusMessage = "";

                    // Insert Invoice Header
                    dataContext.ARInvoicesHDRs.InsertOnSubmit(InvHDR);
                    dataContext.SubmitChanges();

                    break;
            }


            return "COMPLETE";
        }

        public string SAPVoucherMatch()
        {
            SAPbobsCOM.Company company = new SAPbobsCOM.Company();
            int connectionResult;
            company.Server = ""; // ConfigurationManager.AppSettings["server"].ToString();
            company.CompanyDB = ""; // ConfigurationManager.AppSettings["companydb"].ToString();
            company.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
            company.DbUserName = ""; // ConfigurationManager.AppSettings["dbuser"].ToString();
            company.DbPassword = ""; // ConfigurationManager.AppSettings["dbpassword"].ToString();
            company.UserName = ""; // ConfigurationManager.AppSettings["user"].ToString();
            company.Password = ""; // ConfigurationManager.AppSettings["password"].ToString();
            company.language = SAPbobsCOM.BoSuppLangs.ln_English_Gb;
            company.UseTrusted = false;
            company.LicenseServer = ""; // ConfigurationManager.AppSettings["licenseServer"].ToString();

            connectionResult = company.Connect();

            if (connectionResult != 0)
            {
                //company.GetLastError(out errorCode, out errorMessage);
            }

            InternalReconciliationsService service = company.GetCompanyService().GetBusinessService(ServiceTypes.InternalReconciliationsService);
            InternalReconciliationOpenTransParams transParams = service.GetDataInterface(InternalReconciliationsServiceDataInterfaces.irsInternalReconciliationOpenTransParams);
            InternalReconciliationParams reconParams = service.GetDataInterface(InternalReconciliationsServiceDataInterfaces.irsInternalReconciliationParams);
            bool reconcileOnAccount = true;
            int transId1 = 2;
            int transRowId1 = 0;
            int transId2 = 5;
            int transRowId2 = 1;

            // Set date selection criteria
            transParams.ReconDate = DateTime.Today;
            transParams.DateType = ReconSelectDateTypeEnum.rsdtPostDate;
            //transParams.FromDate = new DateTime(2017, 10, 11);
            //transParams.ToDate = new DateTime(2017, 11, 11);

            // Set account or card selection criteria
            if (false == reconcileOnAccount)
            {
                transParams.CardOrAccount = CardOrAccountEnum.coaAccount;
                transParams.AccountNo = "100101";
            }
            else
            {
                transParams.CardOrAccount = CardOrAccountEnum.coaCard;
                transParams.InternalReconciliationBPs.Add();
                transParams.InternalReconciliationBPs.Item(0).BPCode = "v01";
            }

            try
            {
                InternalReconciliationOpenTrans openTrans = service.GetOpenTransactions(transParams);

                // Select transactions to reconcile and adjust amount
                foreach (InternalReconciliationOpenTransRow row in openTrans.InternalReconciliationOpenTransRows)
                {
                    if (transId1 == row.TransId && transRowId1 == row.TransRowId)
                    {
                        row.Selected = BoYesNoEnum.tYES;
                        row.ReconcileAmount = 11;
                        row.CashDiscount = 1;
                    }
                    else if (transId2 == row.TransId && transRowId2 == row.TransRowId)
                    {
                        row.Selected = BoYesNoEnum.tYES;
                        row.ReconcileAmount = 10;
                    }
                }

                // Add reconciliation
                reconParams = service.Add(openTrans);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return "COMPLETE";
        }

        public string MagentaStringToJSON(string InString)
        {
            // Convert string of data from Magenta in JSON string format

            string jsonval = "";
            string firstval = "Y";

            try
            {
                var fieldvals = InString.Split('\u0001');

                foreach (string vals in fieldvals)
                {
                    var sval = vals.Split(':');
                    string fieldname = sval.Length >= 1 && sval[0] != null ? sval[0] : "";
                    string value1 = sval.Length >= 2 && sval[1] != null ? sval[1] : "";
                    string value2 = sval.Length >= 3 && sval[2] != null ? sval[2] : "";
                    string value3 = sval.Length >= 4 && sval[3] != null ? sval[3] : "";
                    string value4 = sval.Length >= 5 && sval[4] != null ? sval[4] : "";
                    string value5 = sval.Length >= 6 && sval[5] != null ? sval[5] : "";
                    value1 = value1.Replace(@"\", @"\\");
                    value2 = value2.Replace(@"\", @"\\");
                    value3 = value3.Replace(@"\", @"\\");
                    value4 = value4.Replace(@"\", @"\\");
                    value5 = value5.Replace(@"\", @"\\");

                    if (firstval == "Y")
                    {
                        jsonval = string.Format("\"{1}\":\"{2}{3}{4}{5}{6}\"", jsonval, fieldname, value1, value2, value3, value4, value5);
                        firstval = "N";
                    }
                    else
                    {
                        jsonval = string.Format("{0},\"{1}\":\"{2}{3}{4}{5}{6}\"", jsonval, fieldname, value1, value2, value3, value4, value5);
                    }
                }

                jsonval = string.Format("{{ {0} }}", jsonval);
            }
            catch (Exception ex)
            {
                Program.log.Error(ex);
            }

            return jsonval;

        }

        public List<Sale> SAPInvoicesToMagenta()
        {
            Sales SalesXML = new Sales();
            List<Sale> ListSaleXML = new List<Sale>();

            try
            {
                Program.log.Info("Begin GET SAP B1 Process: Invoice");

                var query = from vINVOICEHDR in dataContext.vINVOICEHDRs
                            select vINVOICEHDR;

                foreach (vINVOICEHDR sapinv in query)
                {
                    Sale SaleXML = new Sale();
                    List<SaleLine> ListSaleLineXML = new List<SaleLine>();
                    List<TenderLine> ListTenderLineXML = new List<TenderLine>();
                    SaleXML.TranId = sapinv.TranId.ToString();
                    SaleXML.TranDate = sapinv.TranDate ?? DateTime.Now;
                    SaleXML.CustomerCode = sapinv.U_MagCustomerCode;
                    SaleXML.SaleBranchCode = sapinv.SaleBranchCode;
                    SaleXML.StockBranchCode = sapinv.StockBranchCode;

                    var query2 = from vINVOICELINE in dataContext.vINVOICELINEs
                                 where vINVOICELINE.DocEntry == sapinv.DocEntry
                                 select vINVOICELINE;

                    foreach (vINVOICELINE sapinvline in query2)
                    {
                        SaleLine SaleLineXML = new SaleLine();
                        SaleLineXML.SaleLineId = sapinvline.SaleLineId.ToString();
                        SaleLineXML.ProductCode = sapinvline.ProductCode;
                        SaleLineXML.Quantity = sapinvline.Quantity ?? 0;
                        SaleLineXML.UnitSellingPrice = sapinvline.UnitSellingPrice ?? 0;

                        ListSaleLineXML.Add(SaleLineXML);
                    }

                    var query3 = from vINVOICEPAY in dataContext.vINVOICEPAYs
                                 where vINVOICEPAY.DocEntry == sapinv.DocEntry
                                 select vINVOICEPAY;

                    int i = 1;
                    foreach (vINVOICEPAY sappay in query3)
                    {
                        TenderLine TenderLineXML = new TenderLine();
                        TenderLineXML.TenderLineId = i;
                        TenderLineXML.TenderType = sappay.U_MagPaymentMethod;
                        TenderLineXML.TenderAmount = sappay.SumApplied ?? 0;

                        ListTenderLineXML.Add(TenderLineXML);
                        i++;
                    }

                    SaleXML.TenderLine = ListTenderLineXML;
                    SaleXML.SaleLine = ListSaleLineXML;
                    ListSaleXML.Add(SaleXML);
                }

                SalesXML.Sale = ListSaleXML;

                Program.log.Info("End GET SAP B1 Process: Invoice");
            }
            catch (Exception ex)
            {
                Program.log.Error(ex);
            }

            return ListSaleXML;

        }

    }
}
