﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentaToSAPIntegration.Models
{
    public class RecordSupplier
    {
        public string SupplierCode { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string Postcode { get; set; }
        public string State { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
    }

    public class Suppliers
    {
        public bool Processnow { get; set; }
        public string Commssession { get; set; }
        public List<RecordSupplier> Records { get; set; }
    }
}
