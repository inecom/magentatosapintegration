﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentaToSAPIntegration.Models
{
    public class RecordAnalysisCode4
    {
        public string Analysis4code { get; set; }
        public string Description { get; set; }
        public string Analysis3code { get; set; }
    }

    public class AnalysisCode4
    {
        public string Websiteusercode { get; set; }
        public string Websitepassword { get; set; }
        public bool Processnow { get; set; }
        public string Commssession { get; set; }
        public List<RecordAnalysisCode4> Records { get; set; }
    }
}
