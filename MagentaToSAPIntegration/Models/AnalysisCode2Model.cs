﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentaToSAPIntegration.Models
{
    public class RecordAnalysisCode2
    {
        public string Analysis2code { get; set; }
        public string Description { get; set; }
        public string Analysis1code { get; set; }
    }

    public class AnalysisCode2
    {
        public string Websiteusercode { get; set; }
        public string Websitepassword { get; set; }
        public bool Processnow { get; set; }
        public string Commssession { get; set; }
        public List<RecordAnalysisCode2> Records { get; set; }
    }

}
