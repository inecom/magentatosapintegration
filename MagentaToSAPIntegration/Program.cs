﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using MagentaToSAPIntegration.Services;
[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace MagentaToSAPIntegration
{
    static class Program
    {
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            Console.Write("test");
#if DEBUG
            ProcessQueue pq = new ProcessQueue();
            //pq.SAPDataImportMan("TransRecords_Test.JSON");
            pq.SAPOrdtoMagMan("OrdertoMag_Test.xml");
            //pq.MagProcQueueUpdate();
            //pq.MagProcQueueGET();
            //pq.MagProcQueuePOST();

            Console.Write("test");
#else
            if (args.Count() > 0 && args[0] == "RUN")
            {
                ProcessQueue pq = new ProcessQueue();
                pq.MagProcQueueUpdate();
                pq.MagProcQueueGET();
                pq.MagProcQueuePOST();
            }
            else if (args.Count() > 0 && args[0] == "MANUALBATCH")
            {
                ProcessQueue pq = new ProcessQueue();
                string filename = args[1];
                pq.SAPDataImportMan(filename);
            }
            else if (args.Count() > 0 && args[0] == "COMPLETEBATCH")
            {
                ProcessQueue pq = new ProcessQueue();
                string batchid = args[1];
                pq.CompleteFromMagenta("TransRecords", batchid);
            }
            else if (args.Count() > 0 && args[0] == "MANUALORDEXPORT")
            {
                ProcessQueue pq = new ProcessQueue();
                string filename = args[1];
                pq.SAPOrdtoMagMan(filename);
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                        new MagentaToSAPIntegrationService()
                };
                ServiceBase.Run(ServicesToRun);
            }
#endif

        }
    }
}
