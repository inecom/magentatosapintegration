﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using MagentaToSAPIntegration.Models;

namespace MagentaToSAPIntegration.Services
{
    public class SQLData
    {

        #region ExecuteNonQuery
        public void SQLNonQuery(string sqlcmd)
        {
            try
            {
                string cs = ConfigurationManager.ConnectionStrings["MagentaToSAPIntegration.Properties.Settings.MagentaIntegrationConnectionString"].ConnectionString;

                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    SqlCommand command = new SqlCommand(sqlcmd, con);
                    command.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ExecuteScalar
        public object ExecuteScalar(string sqlcmd)
        {
            try
            {
                object response = new object();
                string cs = ConfigurationManager.ConnectionStrings["MagentaToSAPIntegration.Properties.Settings.MagentaIntegrationConnectionString"].ConnectionString;

                using (SqlConnection con = new SqlConnection(cs))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand(sqlcmd, con))
                    {
                        response = command.ExecuteScalar();
                    }
                }

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

    }
}
