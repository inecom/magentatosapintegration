﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentaToSAPIntegration.Models
{
    public class RecordCostsArea
    {
        //public string ProductCode { get; set; }
        public string AreaCode { get; set; }
        public int AreaType { get; set; }
        public double BuyPrice1 { get; set; }
    }

    public class RecordBranchSetPrice
    {
        //public string ProductCode { get; set; }
        public string BranchSetCode { get; set; }
        public double RetailPrice { get; set; }
        public double RRP { get; set; }
        public double FullPrice { get; set; }
    }

    public class RecordBarcode
    {
        //public string ProductCode { get; set; }
        public string NewBarcode { get; set; }
    }

    public class RecordWebsites
    {
        public string WebsiteCode { get; set; }
        public bool Publish { get; set; }
    }

    public class RecordProduct
    {
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public bool Inactive { get; set; }
        public bool Purge { get; set; }
        public bool NonInventory { get; set; }
        public bool SerialNumberRequired { get; set; }
        public string Analysis1Code { get; set; }
        public string Analysis2Code { get; set; }
        public string Analysis3Code { get; set; }
        public string Analysis4Code { get; set; }
        public string Supplier1 { get; set; }
        public string SupplierCode1 { get; set; }
        public string ExternalId1 { get; set; }
        public string ExtraInfo1 { get; set; }
        public int StockedInWarehouse { get; set; }
        public bool OverrideTaxRate { get; set; }
        public double Taxrate { get; set; }
        public List<RecordCostsArea> CostsArea { get; set; }
        public List<RecordBranchSetPrice> BranchSetPrices { get; set; }
        public List<RecordBarcode> Barcodes { get; set; }
        public List<RecordWebsites> Websites { get; set; }
    }

    public class Products
    {
        //public string Websiteusercode { get; set; }
        //public string Websitepassword { get; set; }
        public bool Processnow { get; set; }
        public string Commssession { get; set; }
        public List<RecordProduct> Records { get; set; }
    }

    public class Barcodes
    {
        public bool Processnow { get; set; }
        public string Commssession { get; set; }
        public List<RecordBarcode> Records { get; set; }
    }

    public class CostsArea
    {
        public bool Processnow { get; set; }
        public string Commssession { get; set; }
        public List<RecordCostsArea> Records { get; set; }
    }

    public class BranchSetPrice
    {
        public bool Processnow { get; set; }
        public string Commssession { get; set; }
        public List<RecordBranchSetPrice> Records { get; set; }
    }

    
}
