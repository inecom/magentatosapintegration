﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MagentaToSAPIntegration.Models
{
    public class Tn
    {
        public int recordsequencenumber { get; set; }
        public string recordidentifier { get; set; }
        public string transactionnumber { get; set; }
        public string tendercode { get; set; }
        public double amount { get; set; }
        public string refnumber { get; set; }
        public string expirydate { get; set; }
        public string branch { get; set; }
        public int registercode { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string eftcardtype { get; set; }
        public string eftaccounttype { get; set; }
        public string eftauditnumber { get; set; }
        public string shift { get; set; }
        public string userid { get; set; }
        public string eftsettlementdate { get; set; }
        public bool changerecords { get; set; }
        public double foreigncurrencyamount { get; set; }
        public double foreigncurrencyexchangerate { get; set; }
        public bool voucherissue { get; set; }
        public double eftposcashoutamount { get; set; }
        public int tenderlinenumber { get; set; }
        public string hochargeaccountauthorisation { get; set; }
        public string invoicenumber { get; set; }
        public string userbranch { get; set; }
        public double changeapportionedtothistender { get; set; }
        public string chequedrawername { get; set; }
        public string chequebankname { get; set; }
        public string chequebankbranch { get; set; }
        public string telecheck { get; set; }
        public string phone { get; set; }
        public string authorisationnumber { get; set; }
        public string debtorcode { get; set; }
        public string details { get; set; }
        public int creditcardsurcharge { get; set; }
        public string cardnumbertoken { get; set; }
    }

    public class Sm
    {
        public int recordsequencenumber { get; set; }
        public string recordidentifier { get; set; }
        public string branchcode { get; set; }
        public int registercode { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string userid { get; set; }
        public string reference1 { get; set; }
        public string reference2 { get; set; }
        public string reference3 { get; set; }
        public string reference4 { get; set; }
        public string reference5 { get; set; }
        public int stockmovementtype { get; set; }
        public string productcode { get; set; }
        public string stockbincode { get; set; }
        public double quantity { get; set; }
        public double cost { get; set; }
        public string description { get; set; }
        public string lineid { get; set; }
        public string userbranch { get; set; }
        public bool kitcomponent { get; set; }
        public double oncost { get; set; }
        public bool enablerisineeestockmode { get; set; }
        [JsonProperty("non-inventoryitem")]
        public bool NonInventoryitem { get; set; }
        public int stockvaluationmethod { get; set; }
        public string stocksourcelocation { get; set; }
    }

    public class Sl
    {
        public int recordsequencenumber { get; set; }
        public string recordidentifier { get; set; }
        public int linenumber { get; set; }
        public string branchcode { get; set; }
        public int registercode { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string userid { get; set; }
        public string transactionnumber { get; set; }
        public string invoicenumber { get; set; }
        public string customernumber { get; set; }
        public double quantity { get; set; }
        public double quantitymultiplier { get; set; }
        public double unitprice { get; set; }
        public double unitcost { get; set; }
        public double extendedamount { get; set; }
        public string plu { get; set; }
        public double discountamount { get; set; }
        public double discountpercentage { get; set; }
        public string discountreasoncode { get; set; }
        public string returnreasoncode { get; set; }
        public double gstamountextended { get; set; }
        public double gstrate { get; set; }
        public double salestaxamount { get; set; }
        public double salestaxrate { get; set; }
        public string gstflag { get; set; }
        public string salestaxflag { get; set; }
        public string linestatus { get; set; }
        public double unitpriceinclgst { get; set; }
        public double extendedamountinclgst { get; set; }
        public double discountamountinclgst { get; set; }
        public string divisioncode { get; set; }
        public string departmentcode { get; set; }
        public string classcode { get; set; }
        public string brandcode { get; set; }
        public string categorycode { get; set; }
        public string seasoncode { get; set; }
        public string stylecode { get; set; }
        public string colourcode { get; set; }
        public string sizecode { get; set; }
        public string originallinestatus { get; set; }
        public string customerloyaltynumber { get; set; }
        public string loyaltycardexpirydate { get; set; }
        public string originalinvoicenumber { get; set; }
        public string serialnumber { get; set; }
        public string salespersoncode { get; set; }
        public string tourcode { get; set; }
        public int touroperatorcommissionrate { get; set; }
        public string promotioncode { get; set; }
        public string manualdocketnumber { get; set; }
        public int warrantyduration { get; set; }
        public string repairjobnumber { get; set; }
        public string originalsalebranch { get; set; }
        public string model { get; set; }
        public string serviceplankey { get; set; }
        [JsonProperty("salesub-line")]
        public int SalesubLine { get; set; }
        public string cashierbranch { get; set; }
        public string salespersonbranch { get; set; }
        public bool giftpurchase { get; set; }
        public string discountauthorityoverride { get; set; }
        public string returnauthorityuser { get; set; }
        public string returnauthoritybranch { get; set; }
        public string giftregistrynumber { get; set; }
        public int transactionequipment { get; set; }
        public int attractingline { get; set; }
        public int monthsdeferment { get; set; }
        public string discountsubreasoncode { get; set; }
        public string externalskihirefilename { get; set; }
        public string barcode { get; set; }
        public int weightoffset { get; set; }
        public string originalstore { get; set; }
        public string originalsaledate { get; set; }
        public string scannedbarcode { get; set; }
        public bool floorstock { get; set; }
        public int gtilineindicator { get; set; }
        public string sortkey { get; set; }
    }

    public class Ge
    {
        public int recordsequencenumber { get; set; }
        public string recordidentifier { get; set; }
        public string branchcode { get; set; }
        public int tillcode { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string userid { get; set; }
        public string userbranch { get; set; }
        public string transactionnumber { get; set; }
        public string invoicenumber { get; set; }
        public int unbalanced { get; set; }
        public int numberofrepeatingentries { get; set; }
        public string glaccountcode { get; set; }
        public string glnarration { get; set; }
        public double glamount { get; set; }
        public string glreasontendercode { get; set; }
    }

    public class Rc
    {
        public int recordsequencenumber { get; set; }
        public string recordidentifier { get; set; }
        public string branchcode { get; set; }
        public int registercode { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string userid { get; set; }
        public string transactionnumber { get; set; }
        public string invoicenumber { get; set; }
        public string surname { get; set; }
        public string firstname { get; set; }
        public string title { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string state { get; set; }
        public string postcode { get; set; }
        public string phone { get; set; }
        public string salesperson { get; set; }
        public string userbranch { get; set; }
        public string salespersonbranch { get; set; }
    }

    public class Sixty    {
        public int recordsequencenumber { get; set; }
        public string recordidentifier { get; set; }
        public int linenumber { get; set; }
        public string branchcode { get; set; }
        public int registercode { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string userid { get; set; }
        public string transactionnumber { get; set; }
        public string invoicenumber { get; set; }
        public int kititemnumber { get; set; }
        public string originalsaledate { get; set; }
        public string originalsaletime { get; set; }
        public string originalinvoicenumber { get; set; }
        public int originalsaleline { get; set; }
        public int originalkititemnumber { get; set; }
        public double returnedquantity { get; set; }
        public string userbranchcode { get; set; }
        public string returnreasonextradetail { get; set; }
    }

    public class Md
    {
        public int recordsequencenumber { get; set; }
        public string recordidentifier { get; set; }
        public string transactionnumber { get; set; }
        public int salelinenumber { get; set; }
        public int salesublinenumber { get; set; }
        public int discountlinenumber { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string invoicenumber { get; set; }
        public string branchcode { get; set; }
        public int tillcode { get; set; }
        public int discounttype { get; set; }
        public string discountreasoncode { get; set; }
        public string promotioncode { get; set; }
        public string authoriseduser { get; set; }
        public string authuserbranch { get; set; }
        public double discountamount { get; set; }
        public string discountsubreasoncode { get; set; }
        public string customercategorycode { get; set; }
        public string partnermembershipnumber { get; set; }
        public string loyaltynumber { get; set; }
        public string rewardsvouchernumber { get; set; }
        public string markup { get; set; }
        public string partnerprogramcode { get; set; }
    }

    public class Ic
    {
        public int recordsequencenumber { get; set; }
        public string recordidentifier { get; set; }
        public int linenumber { get; set; }
        public string branchcode { get; set; }
        public int registercode { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string userid { get; set; }
        public string transactionnumber { get; set; }
        public string invoicenumber { get; set; }
        public string commenttext { get; set; }
        public string userbranch { get; set; }
    }

    public class Oc
    {
        public int recordsequencenumber { get; set; }
        public string recordidentifier { get; set; }
        public string branchcode { get; set; }
        public int registercode { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string userid { get; set; }
        public string transactionnumber { get; set; }
        public string vouchernumber { get; set; }
        public string expirydate { get; set; }
        public double issueamount { get; set; }
        public string customername { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string postcode { get; set; }
        public string phone { get; set; }
        public string userbranch { get; set; }
        public string topup { get; set; }
        public string activate { get; set; }
        public double discount { get; set; }
        public string endvouchernumber { get; set; }
        public int numberofvoucherissued { get; set; }
        public string checkdigitpresent { get; set; }
        public string qsoffline { get; set; }
        public string loyaltypointsissue { get; set; }
        public string refundissue { get; set; }
        public string productcode { get; set; }
        public string customercode { get; set; }
        public string lgcredeemedasdiscount { get; set; }
        public string websiteordernumber { get; set; }
        public string issueglexpenseaccount { get; set; }
        public string activationcode { get; set; }
    }

    public class Or
    {
        public int recordsequencenumber { get; set; }
        public string recordidentifier { get; set; }
        public string branchcode { get; set; }
        public int registercode { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string userid { get; set; }
        public string transactionnumber { get; set; }
        public string vouchernumber { get; set; }
        public double redemptionamount { get; set; }
        public string expirydate { get; set; }
        public string userbranch { get; set; }
        public string customername { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string postcode { get; set; }
        public string phone { get; set; }
        public string tendercode { get; set; }
        public string shiftcode { get; set; }
        public string customercode { get; set; }
        public string lgcredeemedasdiscount { get; set; }
        public string dataloaderignorethisrecord { get; set; }
    }

    public class Oh
    {
        public int recordsequencenumber { get; set; }
        public string recordidentifier { get; set; }
        public string ordernumber { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string branch { get; set; }
        public string customercode { get; set; }
        public bool newsale { get; set; }
        public string orderduedate { get; set; }
        public string userbranch { get; set; }
        public int numberofinstalments { get; set; }
        public int instalmentfrequency { get; set; }
        public double regularinstalmentamount { get; set; }
        public double finalinstalmentamount { get; set; }
        public string partnerprogramname { get; set; }
        public string partnerreferralnumber { get; set; }
        public string unused { get; set; }
        public string freetext1 { get; set; }
        public string freetext2 { get; set; }
        public string freetext3 { get; set; }
        public string freetext4 { get; set; }
        public string freetext5 { get; set; }
        public string freetext6 { get; set; }
        public string freetext7 { get; set; }
        public string freetext8 { get; set; }
        public string sourceofsale { get; set; }
        public string subbranchcode { get; set; }
        [JsonProperty("re-export")]
        public bool ReExport { get; set; }
        public int expectedlines { get; set; }
        public string deliveryinstruction1 { get; set; }
        public string deliveryinstruction2 { get; set; }
        public string deliveryname { get; set; }
        public string deliverycompanyname { get; set; }
        public string deliveryaddress1 { get; set; }
        public string deliveryaddress2 { get; set; }
        public string deliveryaddress3 { get; set; }
        public string deliveryaddress4 { get; set; }
        public string deliverystate { get; set; }
        public string deliverypostcode { get; set; }
        public string deliveryisosubdivision { get; set; }
        public string deliveryisocountry { get; set; }
        public string phone1 { get; set; }
        public string deliveryemail { get; set; }
        public bool lockoncreate { get; set; }
        public string customerorderreference { get; set; }
        public string projectname { get; set; }
        public string projectcontact { get; set; }
        public string reference1 { get; set; }
        public string deliverydate { get; set; }
        public int deliverytime { get; set; }
        public string deliverytimenote { get; set; }
        public bool salesextax { get; set; }
    }

    public class Op
    {
        public int recordsequencenumber { get; set; }
        public string recordidentifier { get; set; }
        public string branchcode { get; set; }
        public int registercode { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string userid { get; set; }
        public string transactionnumber { get; set; }
        public string ordernumber { get; set; }
        public double servicefeeamount { get; set; }
        public string lineid { get; set; }
        public string userbranch { get; set; }
    }

    public class Ot
    {
        public int recordsequencenumber { get; set; }
        public string recordidentifier { get; set; }
        public int linenumber { get; set; }
        public string branchcode { get; set; }
        public int registercode { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string ordernumber { get; set; }
        public string userid { get; set; }
        public string transactionnumber { get; set; }
        public string invoicenumber { get; set; }
        public string customernumber { get; set; }
        public double quantity { get; set; }
        public double quantitymultiplier { get; set; }
        public double unitprice { get; set; }
        public double unitcost { get; set; }
        public double extendedamount { get; set; }
        public string plu { get; set; }
        public double discountamount { get; set; }
        public double discountpercentage { get; set; }
        public double gstamount { get; set; }
        public double gstrate { get; set; }
        public string gstflag { get; set; }
        public string linestatus { get; set; }
        public double unitpriceinclgst { get; set; }
        public double extendedamountinclgst { get; set; }
        public double discountamountinclgst { get; set; }
        public string userbranch { get; set; }
        public string serialnumber { get; set; }
        public bool useallocations { get; set; }
        public int splitlinenumber { get; set; }
        public string originalinvoicenumber { get; set; }
        public int originallinenumber { get; set; }
        public int originalkitlinenumber { get; set; }
        public string originalsaledate { get; set; }
        public string lastordertrannumber { get; set; }
        [JsonProperty("re-export")]
        public bool ReExport { get; set; }
        public bool forcedeliveryflag { get; set; }
    }

    public class Uo
    {
        public int recordsequencenumber { get; set; }
        public string recordidentifier { get; set; }
        public string branchcode { get; set; }
        public string tillcode { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string userid { get; set; }
        public string userbranch { get; set; }
        public string transactionnumber { get; set; }
        public string invoicenumber { get; set; }
        public string customerordernumber { get; set; }
    }

    public class Sh
    {
        public int recordsequencenumber { get; set; }
        public string recordidentifier { get; set; }
        public string branchcode { get; set; }
        public string registercode { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string userid { get; set; }
        public string transactionnumber { get; set; }
        public string invoicenumber { get; set; }
        public string laybynumber { get; set; }
        public string customernumber { get; set; }
        public double total { get; set; }
        public double discount { get; set; }
        public double centrounding { get; set; }
        public double chequefee { get; set; }
        public double servicecharges { get; set; }
        public double gst { get; set; }
        public double salestax { get; set; }
        public int numberofdetaillines { get; set; }
        public double totalgstinclusive { get; set; }
        public double chargedtoaccount { get; set; }
        public double chargeaccountpaymentamount { get; set; }
        public double laybypaymentamount { get; set; }
        public double discountgstinclusive { get; set; }
        public string customerordernumber { get; set; }
        public string australiansalestaxflag { get; set; }
        public string taxexemptioncertificatenumber { get; set; }
        public string customerclubcode { get; set; }
        public double clubpointsawardedthistransaction { get; set; }
        public string vipcustomernumber { get; set; }
        public string customerpostcode { get; set; }
        public double gstliability { get; set; }
        public string optionalendoftransactionfield1 { get; set; }
        public string optionalendoftransactionfield2 { get; set; }
        public string staffsurnameforstaffdiscount { get; set; }
        public string stafffirstnameforstaffdiscount { get; set; }
        public string staffpayrollnumberforstaffdisc { get; set; }
        public string flybuysnumber { get; set; }
        public bool flybuysnumberreplaceinhost { get; set; }
        public string rewardcode { get; set; }
        public string optionalendoftransactionfield3 { get; set; }
        public string optionalendoftransactionfield4 { get; set; }
        public string optionalendoftransactionfield5 { get; set; }
        public double globaltaxrate { get; set; }
        public bool errorcorrecttransaction { get; set; }
        public double totaltradeinvalue { get; set; }
        public string tourcode { get; set; }
        public double touroperatorcommissionrate { get; set; }
        public int nuancetransactiontype { get; set; }
        public string nuancesaletypeanalysiscode { get; set; }
        public string salespersoncode { get; set; }
        public string cashierbranch { get; set; }
        public string salespersonbranch { get; set; }
        public bool salecontainsgiftpurchases { get; set; }
        public string externaltransactionfile { get; set; }
        public bool staffuniformsale { get; set; }
        public string voidedinvoicenumber { get; set; }
        public string voidsaledate { get; set; }
        public string shiftcode { get; set; }
        public string optionalendoftransactionfield6 { get; set; }
        public string optionalendoftransactionfield7 { get; set; }
        public string numberofhosfilerows { get; set; }
        public int creditcardsurcharge { get; set; }
        public string partnerprogramname { get; set; }
        public string partnerreferralnumber { get; set; }
        public bool realtimeexportfailureflag { get; set; }
        public string realtimeexporterrorcode { get; set; }
        public int timespentinsalesscreen { get; set; }
        public int timespentintenderscreen { get; set; }
        public string websitecode { get; set; }
        public string sourceofsale { get; set; }
        public string ereceiptaddress { get; set; }
        public List<Tn> tn { get; set; }
        public List<Sm> sm { get; set; }
        public List<Sl> sl { get; set; }
        public Ge ge { get; set; }
        public Rc rc { get; set; }
        [JsonProperty("60")]
        public List<Sixty> Sixty { get; set; }
        public List<Md> md { get; set; }
        public List<Ic> ic { get; set; }
        public List<Oc> oc { get; set; }
        public List<Or> or { get; set; }
        public Oh oh { get; set; }
        public List<Op> op { get; set; }
        public List<Ot> ot { get; set; }
        public Uo uo { get; set; } 
    }

    public class Sixtyone    
    {
        public int recordsequencenumber { get; set; }
        public string recordidentifier { get; set; }
        public string customernvpdata { get; set; }
        [JsonProperty("pass-throughtohlesonly")]
        public bool PassThroughtohlesonly { get; set; } 
    }

    public class Siztytwo    
    {
        public int recordsequencenumber { get; set; }
        public string recordidentifier { get; set; }
        public string customeraddressnvpdata { get; set; }
        [JsonProperty("pass-throughtohlesonly")]
        public bool PassThroughtohlesonly { get; set; } 
    }

    public class Cf
    {
    }

    public class Transactiondetail
    {
        public Sh sh { get; set; }
        [JsonProperty("61")] 
        public Sixtyone Sixtyone { get; set; }
        [JsonProperty("62")] 
        public Siztytwo Siztytwo { get; set; }
        public Cf cf { get; set; } 
    }

    public class ListPosTransactionRecord
    {
        public string Transactionid { get; set; }
        public string Transactiontype { get; set; }
        public Transactiondetail Transactiondetail { get; set; }
    }

    public class TransRecords
    {
        public string BatchId { get; set; }
        public List<ListPosTransactionRecord> ListPosTransactionRecord { get; set; }
    }

}
