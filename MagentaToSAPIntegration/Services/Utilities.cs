﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Xml;
using System.Xml.Serialization;


namespace MagentaToSAPIntegration.Services
{
    class Utilities
    {
        public string unzipbyte(byte[] data)
        {
            // get byte data, and send to decompression
            // decode to string using utf8 format
            byte[] Uncompressed = Unzip(data);
            string output;
            UTF8Encoding enc = new UTF8Encoding();
            output = enc.GetString(Uncompressed);
            Console.WriteLine(output);
            return output;
        }

        static byte[] Unzip(byte[] gzip)
        {
            // Decompression byte value with GZip writing into a memory stream
            using (GZipStream stream = new GZipStream(new MemoryStream(gzip),
                CompressionMode.Decompress))
            {
                const int size = 4096;
                byte[] buffer = new byte[size];
                using (MemoryStream memory = new MemoryStream())
                {
                    int count = 0;
                    do
                    {
                        count = stream.Read(buffer, 0, size);
                        if (count > 0)
                        {
                            memory.Write(buffer, 0, count);
                        }
                    }
                    while (count > 0);
                    return memory.ToArray();
                }
            }
        }

        public static byte[] GCompress(string uncompressed)
        {
            using (MemoryStream output = new MemoryStream())
            {
                using (GZipStream gz = new GZipStream(output, CompressionMode.Compress))
                {
                    using (StreamWriter writer = new StreamWriter(gz, Encoding.UTF8))
                    {
                        writer.Write(uncompressed);
                    }
                }
                return output.ToArray();
            }
        }

        public static string GDecompress(byte[] compressed)
        {
            using (MemoryStream inputStream = new MemoryStream(compressed))
            {
                using (GZipStream gzip = new GZipStream(inputStream, CompressionMode.Decompress))
                {
                    using (StreamReader reader = new StreamReader(gzip, Encoding.UTF8))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
        }

        public T Deserialize<T>(string input) where T : class
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(input))
            {
                return (T)ser.Deserialize(sr);
            }
        }

        public string Serialize<T>(T ObjectToSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(ObjectToSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, ObjectToSerialize);
                return textWriter.ToString();
            }
        }

        public static string GetXMLFromObject(object o)
        {
            StringWriter sw = new StringWriter();
            XmlTextWriter tw = null;
            try
            {
                XmlSerializerNamespaces ser = new XmlSerializerNamespaces();
                ser.Add("", "");
                XmlSerializer serializer = new XmlSerializer(o.GetType());
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, o, ser);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                sw.Close();
                if (tw != null)
                {
                    tw.Close();
                }
            }
            return sw.ToString();
        }

        public static object GetObjectFromXML(string xml, Type objectType, XmlRootAttribute rootattrib)
        {
            StringReader sr = null;
            XmlSerializer serializer = null;
            XmlTextReader xmlReader = null;
            Object obj = null;
            try
            {
                sr = new StringReader(xml);
                serializer = new XmlSerializer(objectType, rootattrib);
                xmlReader = new XmlTextReader(sr);
                obj = serializer.Deserialize(xmlReader);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (xmlReader != null)
                {
                    xmlReader.Close();
                }
                if (sr != null)
                {
                    sr.Close();
                }
            }
            return obj;
        }

        public void CreateJSONFile(string dir, string filename, string JSONData)
        {
            string fullpath = dir + filename;
            if (!File.Exists(fullpath))
            {
                // Create a JSON file output with string
                using (StreamWriter sw = File.CreateText(fullpath))
                {
                    sw.WriteLine(JSONData);
                }
            }
        }

        public void CreateXMLFile(string dir, string filename, string XMLData)
        {
            string fullpath = dir + filename;
            if (!File.Exists(fullpath))
            {
                // Create a JSON file output with string
                using (StreamWriter sw = File.CreateText(fullpath))
                {
                    sw.WriteLine(XMLData);
                }
            }
        }

        public static string Left(string value, int length)
        {
            if (string.IsNullOrEmpty(value))
                return null;

            return value.Length < length ? value : value.Substring(0, length);
        }
    }
}
