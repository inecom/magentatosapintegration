﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using MagentaToSAPIntegration.Models;

namespace MagentaToSAPIntegration.Services
{
    partial class MagentaToSAPIntegrationService : ServiceBase
    {
        public Timer timer = new Timer();
        public static bool processrunning = false;

        public MagentaToSAPIntegrationService()
        {
            InitializeComponent();
        }

        protected void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            // Check if process running, otherwise start process based on the Run Timer
            if (processrunning == false)
            {
                processrunning = true;
                ProcessQueue pq = new ProcessQueue();
                Utilities ut = new Utilities();

                pq.MagProcQueueUpdate();
                pq.MagProcQueueGET();
                pq.MagProcQueuePOST();
                processrunning = false;
            }
        }

        public void RunTimer()
        {
            // Looping timer for the service
            int TimerLength = Convert.ToInt32(Properties.Settings.Default.TimerLength);
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            timer.Interval = (TimerLength);
            timer.AutoReset = true;
            timer.Enabled = true;
            timer.Start();

        }

        protected override void OnStart(string[] args)
        {
            // On Service Start create log reference
            try
            {
                Program.log.Info("Magenta to SAP Integration Service Started");
                RunTimer();
            }
            catch (Exception ex)
            {
                Program.log.Error("Magenta to SAP Integration Service Error While Starting");
                Program.log.Error(ex);
            }
        }

        protected override void OnStop()
        {
            // On Service Stop create log reference
            try
            {
                Program.log.Info("Magenta to SAP Integration Service Stopped");
            }
            catch (Exception ex)
            {
                Program.log.Error("Magenta to SAP Integration Service Error While Stopping");
                Program.log.Error(ex);
            }
        }
    }
}
