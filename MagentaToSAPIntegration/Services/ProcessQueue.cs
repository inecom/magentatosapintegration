﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using MagentaToSAPIntegration.Models;
using Newtonsoft.Json;
using MagentaToSAPIntegration.MagentaWebService;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.ServiceModel;
using System.Xml.Linq;

namespace MagentaToSAPIntegration.Services
{
    public class ProcessQueue : IDisposable
    {
        private SQLServerDataDataContext dataContext = new SQLServerDataDataContext();

        public void MagProcQueueUpdate()
        {
            SQLData sqldata = new SQLData();
            try
            {
                // Update Queue Status
                sqldata.SQLNonQuery("EXEC dbo.MAGUpdateQueue");
            }
            catch (Exception ex)
            {
                Program.log.Error(string.Format("Error: {0}", ex.Message));
            }

        }

        public void MagProcQueuePOST()
        {
            SQLData sqldata = new SQLData();

            //Program.log.Info("Begin POST SAP B1 Process: ProcQueue");

            var query = from MAGPROCESSQUEUE in dataContext.MAGPROCESSQUEUEs
                        where MAGPROCESSQUEUE.QueueStatus == Convert.ToChar("Y")
                            && MAGPROCESSQUEUE.ProcessType == "POST"
                        select MAGPROCESSQUEUE;

            //Program.log.Info("End POST SAP B1 Process: ProcQueue");
            
            foreach (MAGPROCESSQUEUE procqueue in query)
            {
                try
                {
                    string processname = procqueue.ProcessName;
                    object jsondata = null;
                    string ID = procqueue.ID.ToString();
                    bool failed = false;
                    DateTime processtart = DateTime.Now;
                    string sqlprocesstart = processtart.ToString("yyyy-MM-dd HH:mm");
                    DateTime DateFrom = Convert.ToDateTime(procqueue.DateFrom);

                    sqldata.SQLNonQuery(string.Format("UPDATE dbo.MAGPROCESSQUEUE SET QueueStatus = 'I', LastProcessMsg = '' WHERE ID = {0}", ID));

                    // GET Data from SAP And Convert For Magenta
                    jsondata = SAPDataExport(processname, DateFrom);

                    // POST to Magenta REST
                    if (jsondata == null && processname != "InvoiceOut")
                    {
                        // Update Queue Status
                        sqldata.SQLNonQuery(string.Format("UPDATE dbo.MAGPROCESSQUEUE SET QueueStatus = 'E', LastProcessed = GETDATE(), LastProcessMsg = 'No Data Returned From Conversion' WHERE ID = {0}", ID));
                    }
                    else if (processname == "InvoiceOut")
                    {
                        sqldata.SQLNonQuery(string.Format("UPDATE dbo.MAGPROCESSQUEUE SET QueueStatus = 'N', LastProcessed = GETDATE(), NextDateFrom = '{1}', LastProcessMsg = 'Data Imported Successfully' WHERE ID = {0}", ID, sqlprocesstart));
                    }
                    else //if (failed == false)
                    {
                        string response = PostToMagenta(jsondata, processname);

                        // Update Queue Status
                        sqldata.SQLNonQuery(string.Format("UPDATE dbo.MAGPROCESSQUEUE SET QueueStatus = 'N', LastProcessed = GETDATE(), NextDateFrom = '{1}', LastProcessMsg = 'Data Imported Successfully' WHERE ID = {0}", ID, sqlprocesstart));
                    }


                }
                catch (Exception ex)
                {
                    Program.log.Error(string.Format("Error: {0}", ex.Message));
                }
            }
        }

        public void MagProcQueueGET()
        {
            SQLData sqldata = new SQLData();

            //Program.log.Info("Begin GET SAP B1 Process: ProcQueue");

            var query = from MAGPROCESSQUEUE in dataContext.MAGPROCESSQUEUEs
                        where MAGPROCESSQUEUE.QueueStatus == Convert.ToChar("Y")
                            && MAGPROCESSQUEUE.ProcessType == "GET"
                        select MAGPROCESSQUEUE;

            //Program.log.Info("End GET SAP B1 Process: ProcQueue");

            foreach (MAGPROCESSQUEUE procqueue in query)
            {
                try
                {
                    string processname = procqueue.ProcessName;
                    object jsondata = null;
                    string ID = procqueue.ID.ToString();
                    bool failed = false;
                    DateTime processtart = DateTime.Now;
                    string sqlprocesstart = processtart.ToString("yyyy-MM-dd HH:mm");
                    DateTime DateFrom = Convert.ToDateTime(procqueue.DateFrom);

                    sqldata.SQLNonQuery(string.Format("UPDATE dbo.MAGPROCESSQUEUE SET QueueStatus = 'I', LastProcessMsg = '' WHERE ID = {0}", ID));

                    // GET Data from SAP And Convert For Magenta
                    jsondata = GetFromMagenta(processname);

                    // POST to Magenta REST
                    if (jsondata == null)
                    {
                        // Update Queue Status
                        sqldata.SQLNonQuery(string.Format("UPDATE dbo.MAGPROCESSQUEUE SET QueueStatus = 'E', LastProcessed = GETDATE(), LastProcessMsg = 'No Data Returned From Conversion' WHERE ID = {0}", ID));
                    }
                    else if (jsondata.ToString() == "No Data To Process")
                    {
                        // Update Queue Status
                        sqldata.SQLNonQuery(string.Format("UPDATE dbo.MAGPROCESSQUEUE SET QueueStatus = 'N', LastProcessed = GETDATE(), LastProcessMsg = 'No Data To Process' WHERE ID = {0}", ID));
                    }
                    else if (jsondata.ToString() == "Data Call Failure")
                    {
                        // Update Queue Status
                        sqldata.SQLNonQuery(string.Format("UPDATE dbo.MAGPROCESSQUEUE SET QueueStatus = 'E', LastProcessed = GETDATE(), LastProcessMsg = 'Failure In Data Call to Magenta' WHERE ID = {0}", ID));
                    }
                    else //if (failed == false)
                    {
                        string response = SAPDataImport(processname, jsondata);

                        // Update Queue Status
                        sqldata.SQLNonQuery(string.Format("UPDATE dbo.MAGPROCESSQUEUE SET QueueStatus = 'N', LastProcessed = GETDATE(), NextDateFrom = '{1}', LastProcessMsg = 'Data Imported Successfully' WHERE ID = {0}", ID, sqlprocesstart));
                    }


                }
                catch (Exception ex)
                {
                    Program.log.Error(string.Format("Error: {0}", ex.Message));
                }
            }
        }

        public object SAPDataExport(string processname, DateTime DateFrom)
        {
            object query = null;
            DataTransform dt = new DataTransform();

            try
            {

                Program.log.Info(string.Format("Begin GET SAP B1 Process: {0}", processname));

                // Get Data from SAP based on the Process Name
                switch (processname)
                {
                    case "Products":
                        List<RecordProduct> MagProducts = new List<RecordProduct>();
                        MagProducts = dt.SAPProductsToMagenta(DateFrom);
                        query = MagProducts;
                        break;
                    case "Suppliers":
                        List<RecordSupplier> MagSuppliers = new List<RecordSupplier>();
                        MagSuppliers = dt.SAPSuppliersToMagenta(DateFrom);
                        query = MagSuppliers;
                        break;
                    case "AnalysisCode1":
                        List<RecordAnalysisCode1> MagAnalysisCode1 = new List<RecordAnalysisCode1>();
                        MagAnalysisCode1 = dt.SAPAnalysisCode1ToMagenta();
                        query = MagAnalysisCode1;
                        break;
                    case "AnalysisCode2":
                        List<RecordAnalysisCode2> MagAnalysisCode2 = new List<RecordAnalysisCode2>();
                        MagAnalysisCode2 = dt.SAPAnalysisCode2ToMagenta();
                        query = MagAnalysisCode2;
                        break;
                    case "AnalysisCode3":
                        List<RecordAnalysisCode3> MagAnalysisCode3 = new List<RecordAnalysisCode3>();
                        MagAnalysisCode3 = dt.SAPAnalysisCode3ToMagenta();
                        query = MagAnalysisCode3;
                        break;
                    case "AnalysisCode4":
                        List<RecordAnalysisCode4> MagAnalysisCode4 = new List<RecordAnalysisCode4>();
                        MagAnalysisCode4 = dt.SAPAnalysisCode4ToMagenta();
                        query = MagAnalysisCode4;
                        break;
                    case "BranchSetPrice":
                        List<RecordBranchSetPrice> MagBranchSetPrice = new List<RecordBranchSetPrice>();
                        MagBranchSetPrice = dt.SAPBranchPriceToMagenta(DateFrom);
                        query = MagBranchSetPrice;
                        break;
                    case "CostArea":
                        List<RecordCostsArea> MagCostsArea = new List<RecordCostsArea>();
                        MagCostsArea = dt.SAPCostAreaToMagenta(DateFrom);
                        query = MagCostsArea;
                        break;
                    case "Barcode":
                        List<RecordBarcode> MagBarcode = new List<RecordBarcode>();
                        MagBarcode = dt.SAPBarcodesToMagenta(DateFrom);
                        query = MagBarcode;
                        break;
                    case "CustomerOut":
                        List<RecordCustomerToMagenta> MagCustomer = new List<RecordCustomerToMagenta>();
                        MagCustomer = dt.SAPCustomerToMagenta(DateFrom);
                        query = MagCustomer;
                        break;
                    case "InvoiceOut":
                        SAPOrdtoMag();
                        break;
                }

                Program.log.Info(string.Format("End GET SAP B1 Process: {0}", processname));

            }
            catch (Exception ex)
            {
                Program.log.Error(ex);
            }

            return query;
        }


        public string PostToMagenta(object jsondata, string processname)
        {
            string jsondatareturn = "";
            object jsondataset = null; ;
            var response = new HttpResponseMessage();
            string BaseURL = Properties.Settings.Default.MagRESTServer;
            string MagUserAuth = Properties.Settings.Default.MagRESTUserAuth;
            string MagPassAuth = Properties.Settings.Default.MagRESTPassAuth;
            Utilities ut = new Utilities();
            string CreateJSONFile = Properties.Settings.Default.GenerateJSONFile == null ? "N" : Properties.Settings.Default.GenerateJSONFile;

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(BaseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = TimeSpan.FromMinutes(30);
                    string apiurl = "";

                    Program.log.Info(string.Format("Begin Magenta REST POST for Process: {0}", processname));

                    switch (processname)
                    {
                        case "Products":
                            apiurl = "stage/productmaster";
                            break;
                        case "Suppliers":
                            apiurl = "stage/supplier";
                            break;
                        case "AnalysisCode1":
                            apiurl = "stage/analysis1";
                            break;
                        case "AnalysisCode2":
                            apiurl = "stage/analysis2";
                            break;
                        case "AnalysisCode3":
                            apiurl = "stage/analysis3";
                            break;
                        case "AnalysisCode4":
                            apiurl = "stage/analysis4";
                            break;
                        case "BranchSetPrice":
                            apiurl = "stage/branchsetprice";
                            break;
                        case "CostArea":
                            apiurl = "stage/costarea";
                            break;
                        case "Barcode":
                            apiurl = "stage/barcode";
                            break;
                        case "CustomerOut":
                            apiurl = "stage/customer";
                            break;
                    }


                    apiurl = String.Format("{0}?Websiteusercode={1}&Websitepassword={2}", apiurl, MagUserAuth, MagPassAuth);

                    int n = 1;
                    int imax = 1000;
                    string commsession = "";

                    //switch (processname)
                    //{
                    //    case "Products":
                    //        imax = Convert.ToInt32(Properties.Settings.Default.ProdRecordMax);
                    //        commsession = "0000000001";
                    //        break;
                    //    case "Suppliers":
                    //        imax = Convert.ToInt32(Properties.Settings.Default.SuppRecordMax);
                    //        commsession = "0000000001";
                    //        break;
                    //    case "AnalysisCode":
                    //        imax = Convert.ToInt32(Properties.Settings.Default.ACRecordMax);
                    //        commsession = "0000000001";
                    //        break;
                    //}

                    List<RecordProduct> prodobj = new List<RecordProduct>();
                    List<RecordSupplier> suppobj = new List<RecordSupplier>();
                    List<RecordAnalysisCode1> ac1obj = new List<RecordAnalysisCode1>();
                    List<RecordAnalysisCode2> ac2obj = new List<RecordAnalysisCode2>();
                    List<RecordAnalysisCode3> ac3obj = new List<RecordAnalysisCode3>();
                    List<RecordAnalysisCode4> ac4obj = new List<RecordAnalysisCode4>();
                    List<RecordBranchSetPrice> bpriceobj = new List<RecordBranchSetPrice>();
                    List<RecordCostsArea> costsobj = new List<RecordCostsArea>();
                    List<RecordBarcode> bcodeobj = new List<RecordBarcode>();
                    List<RecordCustomerToMagenta> custobj = new List<RecordCustomerToMagenta>();
                    var j = jsondata as IEnumerable<object>;
                    int jmax = j.Count();
                    int nj = 1;
                    string running = "N";

                    foreach (var jd1 in jsondata as IEnumerable<object>)
                    {

                        switch (processname)
                        {
                            case "Products":
                                prodobj.Add((RecordProduct)jd1);

                                if (n >= imax || nj >= jmax)
                                {
                                    Products MagProducts = new Products();
                                    MagProducts.Processnow = true;
                                    MagProducts.Commssession = commsession;
                                    MagProducts.Records = prodobj;
                                    jsondataset = MagProducts;
                                }
                                break;
                            case "Suppliers":
                                suppobj.Add((RecordSupplier)jd1);

                                if (n >= imax || nj >= jmax)
                                {
                                    Suppliers MagSuppliers = new Suppliers();
                                    MagSuppliers.Processnow = true;
                                    MagSuppliers.Commssession = commsession;
                                    MagSuppliers.Records = suppobj;
                                    jsondataset = MagSuppliers;
                                }
                                break;
                            case "AnalysisCode1":
                                ac1obj.Add((RecordAnalysisCode1)jd1);

                                if (n >= imax || nj >= jmax)
                                {
                                    AnalysisCode1 MagAnalysisCode1 = new AnalysisCode1();
                                    MagAnalysisCode1.Processnow = true;
                                    MagAnalysisCode1.Commssession = commsession;
                                    MagAnalysisCode1.Records = ac1obj;
                                    jsondataset = MagAnalysisCode1;
                                }
                                break;
                            case "AnalysisCode2":
                                ac2obj.Add((RecordAnalysisCode2)jd1);

                                if (n >= imax || nj >= jmax)
                                {
                                    AnalysisCode2 MagAnalysisCode2 = new AnalysisCode2();
                                    MagAnalysisCode2.Processnow = true;
                                    MagAnalysisCode2.Commssession = commsession;
                                    MagAnalysisCode2.Records = ac2obj;
                                    jsondataset = MagAnalysisCode2;
                                }
                                break;
                            case "AnalysisCode3":
                                ac3obj.Add((RecordAnalysisCode3)jd1);

                                if (n >= imax || nj >= jmax)
                                {
                                    AnalysisCode3 MagAnalysisCode3 = new AnalysisCode3();
                                    MagAnalysisCode3.Processnow = true;
                                    MagAnalysisCode3.Commssession = commsession;
                                    MagAnalysisCode3.Records = ac3obj;
                                    jsondataset = MagAnalysisCode3;
                                }
                                break;
                            case "AnalysisCode4":
                                ac4obj.Add((RecordAnalysisCode4)jd1);

                                if (n >= imax || nj >= jmax)
                                {
                                    AnalysisCode4 MagAnalysisCode4 = new AnalysisCode4();
                                    MagAnalysisCode4.Processnow = true;
                                    MagAnalysisCode4.Commssession = commsession;
                                    MagAnalysisCode4.Records = ac4obj;
                                    jsondataset = MagAnalysisCode4;
                                }
                                break;
                            case "BranchSetPrice":
                                bpriceobj.Add((RecordBranchSetPrice)jd1);

                                if (n >= imax || nj >= jmax)
                                {
                                    BranchSetPrice MagBranchSetPrice = new BranchSetPrice();
                                    MagBranchSetPrice.Processnow = true;
                                    MagBranchSetPrice.Commssession = commsession;
                                    MagBranchSetPrice.Records = bpriceobj;
                                    jsondataset = MagBranchSetPrice;
                                }
                                break;
                            case "CostArea":
                                costsobj.Add((RecordCostsArea)jd1);

                                if (n >= imax || nj >= jmax)
                                {
                                    CostsArea MagCostsArea = new CostsArea();
                                    MagCostsArea.Processnow = true;
                                    MagCostsArea.Commssession = commsession;
                                    MagCostsArea.Records = costsobj;
                                    jsondataset = MagCostsArea;
                                }
                                break;
                            case "Barcode":
                                bcodeobj.Add((RecordBarcode)jd1);

                                if (n >= imax || nj >= jmax)
                                {
                                    Barcodes MagBarcodes = new Barcodes();
                                    MagBarcodes.Processnow = true;
                                    MagBarcodes.Commssession = commsession;
                                    MagBarcodes.Records = bcodeobj;
                                    jsondataset = MagBarcodes;
                                }
                                break;
                            case "CustomerOut":
                                custobj.Add((RecordCustomerToMagenta)jd1);

                                if (n >= imax || nj >= jmax)
                                {
                                    CustomerToMagenta MagCustomer = new CustomerToMagenta();
                                    MagCustomer.Processnow = true;
                                    MagCustomer.Commssession = commsession;
                                    MagCustomer.Records = custobj;
                                    jsondataset = MagCustomer;
                                }
                                break;
                        }

                        if (n >= imax || nj >= jmax)
                        {
                            running = "Y";
                            string outputstring = JsonConvert.SerializeObject(jsondataset);
                            string outputfile = String.Format("{0}_{1}.JSON", processname, DateTime.Now.ToString("yyyyMMddHHmmss"));
                            string outputdir = AppContext.BaseDirectory + "JSONFiles\\";

                            if (CreateJSONFile == "Y")
                                ut.CreateJSONFile(outputdir, outputfile, outputstring);
                            
                            response = client.PostAsJsonAsync(apiurl, jsondataset).Result;

                            if (response.IsSuccessStatusCode)
                            {
                                jsondatareturn = response.Content.ReadAsStringAsync().Result;
                                //List<SuccessFailMsgs> SuccessMsgs = JsonConvert.DeserializeObject<List<SuccessFailMsgs>>(jsondatareturn);
                                //MsgLog(SuccessMsgs, processname);
                                running = "N";
                            }
                            else
                            {
                                jsondatareturn = response.Content.ReadAsStringAsync().Result;

                                outputfile = String.Format("{0}_{1}.JSON", processname, DateTime.Now.ToString("yyyyMMddHHmmss"));
                                outputdir = AppContext.BaseDirectory + "JSONErrorFiles\\";

                                if (CreateJSONFile == "Y")
                                    ut.CreateJSONFile(outputdir, outputfile, outputstring);

                                running = "N";
                            }

                            prodobj.Clear();
                            suppobj.Clear();
                            ac1obj.Clear();
                            ac2obj.Clear();
                            ac3obj.Clear();
                            ac4obj.Clear();
                            bpriceobj.Clear();
                            costsobj.Clear();
                            bcodeobj.Clear();
                            custobj.Clear();
                            jsondataset = null;
                            n = 0;
                        }

                        n++;
                        nj++;
                    }

                    Program.log.Info(string.Format("End Magenta REST POST for REST Process: {0}", processname));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return "done";
        }

        public object GetFromMagenta(string processname)
        {
            string jsondatareturn = "";
            object jsondataset = null; ;
            var response = new HttpResponseMessage();
            string BaseURL = Properties.Settings.Default.MagRESTServer;
            string MagUserAuth = Properties.Settings.Default.MagRESTUserAuth;
            string MagPassAuth = Properties.Settings.Default.MagRESTPassAuth;
            string MagRESTZip = Properties.Settings.Default.MagRESTZip;
            string TransRecordMax = Properties.Settings.Default.TransRecordMax;
            Utilities ut = new Utilities();
            string CreateJSONFile = Properties.Settings.Default.GenerateRawJSONFile == null ? "N" : Properties.Settings.Default.GenerateRawJSONFile;

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(BaseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = TimeSpan.FromMinutes(30);
                    string apiurl = "";

                    Program.log.Info(string.Format("Begin Magenta REST POST for Process: {0}", processname));

                    switch (processname)
                    {
                        case "TransRecords":
                            apiurl = string.Format("postransactionbatch/{0}/{1}", MagRESTZip, TransRecordMax);
                            break;
                    }

                    apiurl = String.Format("{0}?Websiteusercode={1}&Websitepassword={2}", apiurl, MagUserAuth, MagPassAuth);

                    response = client.GetAsync(apiurl).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        jsondatareturn = response.Content.ReadAsStringAsync().Result;

                        string outputstring = jsondatareturn;
                        string outputfile = String.Format("{0}_{1}_{2}.JSON", processname, "Raw", DateTime.Now.ToString("yyyyMMddHHmmss"));
                        string outputdir = AppContext.BaseDirectory + "RawJSONFiles\\";

                        if (CreateJSONFile == "Y")
                            ut.CreateJSONFile(outputdir, outputfile, outputstring);
                    }
                    else
                    {
                        jsondatareturn = response.Content.ReadAsStringAsync().Result;
                    }

                    if (jsondatareturn == "null" && response.IsSuccessStatusCode)
                    {
                        jsondataset = "No Data To Process";
                    }
                    else if (jsondatareturn == "null" && !(response.IsSuccessStatusCode))
                    {
                        jsondataset = "Data Call Failure";
                    }
                    else
                    {
                        switch (processname)
                        {
                            case "TransRecords":
                                TransRecords tr = new TransRecords();
                                tr = JsonConvert.DeserializeObject<TransRecords>(jsondatareturn);
                                jsondataset = tr;
                                break;
                        }
                    }

                    Program.log.Info(string.Format("End Magenta REST POST for REST Process: {0}", processname));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return jsondataset;
        }

        public string CompleteFromMagenta(string processname, string batchid)
        {
            string jsondatareturn = "";
            object jsondataset = null; ;
            var response = new HttpResponseMessage();
            string BaseURL = Properties.Settings.Default.MagRESTServer;
            string MagUserAuth = Properties.Settings.Default.MagRESTUserAuth;
            string MagPassAuth = Properties.Settings.Default.MagRESTPassAuth;
            string MagRESTZip = Properties.Settings.Default.MagRESTZip;
            string TransRecordMax = Properties.Settings.Default.TransRecordMax;
            SQLData sqldata = new SQLData();

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(BaseURL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.Timeout = TimeSpan.FromMinutes(30);
                    string apiurl = "";

                    Program.log.Info(string.Format("Begin Magenta Completion REST POST for Process: {0}", processname));

                    switch (processname)
                    {
                        case "TransRecords":
                            apiurl = string.Format("postransactionbatchack/{0}", batchid);
                            break;
                    }


                    apiurl = String.Format("{0}?Websiteusercode={1}&Websitepassword={2}", apiurl, MagUserAuth, MagPassAuth);

                    response = client.PostAsync(apiurl, null).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        jsondatareturn = response.Content.ReadAsStringAsync().Result;
                        sqldata.SQLNonQuery(string.Format("UPDATE dbo.MagImportBatch SET Status = 'Success' WHERE BatchID = '{0}' ", batchid));
                    }
                    else
                    {
                        jsondatareturn = response.Content.ReadAsStringAsync().Result;
                        sqldata.SQLNonQuery(string.Format("UPDATE dbo.MagImportBatch SET Status = 'Failed' WHERE BatchID = '{0}' ", batchid));
                    }


                    Program.log.Info(string.Format("End Magenta Completion REST POST for REST Process: {0}", processname));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return jsondatareturn;
        }


        public string SAPDataImport(string processname, object jsondata)
        {
            object query = null;
            DataTransform dt = new DataTransform();
            TransRecords MagTransRecords = new TransRecords();
            string jsonstring = "";
            string BatchID = "";
            string TranID = "";
            object BatchCheck;
            int BatchCount = 0;
            SQLData sqldata = new SQLData();
            Utilities ut = new Utilities();
            string CreateJSONFile = Properties.Settings.Default.GenerateJSONFile == null ? "N" : Properties.Settings.Default.GenerateJSONFile;

            try
            {

                Program.log.Info(string.Format("Begin GET Magenta Process: {0}", processname));

                // Get Data from SAP based on the Process Name
                switch (processname)
                {
                    case "TransRecords":
                        MagTransRecords = (TransRecords)jsondata;
                        break;
                }
                
                BatchID = MagTransRecords.BatchId;
                BatchCheck = sqldata.ExecuteScalar(string.Format("SELECT COUNT(*) FROM dbo.MagImportBatch WHERE BatchID = '{0}' ", BatchID));
                if (BatchCheck == null || BatchCheck is DBNull)
                    BatchCount = 0;
                else
                    BatchCount = Convert.ToInt32(BatchCheck);

                string outputstring = JsonConvert.SerializeObject(MagTransRecords);
                string outputfile = String.Format("{0}_{1}_{2}.JSON", processname, BatchID, DateTime.Now.ToString("yyyyMMddHHmmss"));
                string outputdir = AppContext.BaseDirectory + "JSONFiles\\";

                if (CreateJSONFile == "Y")
                    ut.CreateJSONFile(outputdir, outputfile, outputstring);

                if (BatchCount > 0)
                {
                    sqldata.SQLNonQuery(string.Format("INSERT INTO dbo.MagImportBatch(ImportDate, BatchID, Status) VALUES(GETDATE(), '{0}', 'Duplicate') ", BatchID));
                    Program.log.Info(string.Format("Duplicate Batch: {1} In GET Magenta Process: {0}", processname, BatchID));
                }
                else 
                {
                    sqldata.SQLNonQuery(string.Format("INSERT INTO dbo.MagImportBatch(ImportDate, BatchID) VALUES(GETDATE(), '{0}') ", BatchID));

                    foreach (ListPosTransactionRecord PosRecord in MagTransRecords.ListPosTransactionRecord)
                    {
                        Transactiondetail TranDetail = new Transactiondetail();
                        switch (PosRecord.Transactiontype)
                        {
                            case "Sale":
                                TranID = PosRecord.Transactionid;
                                TranDetail = PosRecord.Transactiondetail;
                                string saleresp = dt.SAPOrderTransactions(TranDetail.sh, BatchID, TranID);

                                break;
                            case "61":
                                MagentaCustomer MagCustomer61 = new MagentaCustomer();
                                BPCustomer SAPCustomer61 = new BPCustomer();
                                TranDetail = PosRecord.Transactiondetail;
                                TranID = PosRecord.Transactionid;
                                jsonstring = dt.MagentaStringToJSON(TranDetail.Sixtyone.customernvpdata);
                                MagCustomer61 = JsonConvert.DeserializeObject<MagentaCustomer>(jsonstring);
                                SAPCustomer61 = dt.SAPCustomer(MagCustomer61, BatchID, TranID, PosRecord.Transactiontype);

                                break;
                            case "62":
                                MagentaCustomer MagCustomer62 = new MagentaCustomer();
                                BPCustomer SAPCustomer62 = new BPCustomer();
                                TranDetail = PosRecord.Transactiondetail;
                                TranID = PosRecord.Transactionid;
                                jsonstring = dt.MagentaStringToJSON(TranDetail.Siztytwo.customeraddressnvpdata);
                                MagCustomer62 = JsonConvert.DeserializeObject<MagentaCustomer>(jsonstring);
                                SAPCustomer62 = dt.SAPCustomer(MagCustomer62, BatchID, TranID, PosRecord.Transactiontype);

                                break;
                            case "CF":
                                TranDetail = PosRecord.Transactiondetail;
                                break;
                            case "UP":
                                TranDetail = PosRecord.Transactiondetail;
                                break;
                            case "SA":
                                TranDetail = PosRecord.Transactiondetail;
                                break;
                        }
                    }

                    string compresponse = CompleteFromMagenta(processname, BatchID);
                    //dataContext.sp_ProcessSAP();
                }


                Program.log.Info(string.Format("End GET Magenta Process: {0}", processname));

            }
            catch (Exception ex)
            {
                Program.log.Error(ex);
            }

            return "COMPLETE";
        }

        public string SAPDataImportMan(string filename)
        {
            string fulldir = string.Format("{0}\\{1}", Directory.GetCurrentDirectory(), filename);
            object jsondata;
            string jsondatareturn = File.ReadAllText(@fulldir);
            TransRecords tr = new TransRecords();
            tr = JsonConvert.DeserializeObject<TransRecords>(jsondatareturn);
            jsondata = tr;

            string processname = "TransRecords";

            object query = null;
            DataTransform dt = new DataTransform();
            TransRecords MagTransRecords = new TransRecords();
            string jsonstring = "";
            string BatchID = "";
            string TranID = "";
            SQLData sqldata = new SQLData();
            Utilities ut = new Utilities();
            string CreateJSONFile = Properties.Settings.Default.GenerateJSONFile == null ? "N" : Properties.Settings.Default.GenerateJSONFile;

            try
            {

                // Get Data from SAP based on the Process Name
                switch (processname)
                {
                    case "TransRecords":
                        MagTransRecords = (TransRecords)jsondata;
                        break;
                }

                BatchID = MagTransRecords.BatchId;

                foreach (ListPosTransactionRecord PosRecord in MagTransRecords.ListPosTransactionRecord)
                {
                    Transactiondetail TranDetail = new Transactiondetail();
                    switch (PosRecord.Transactiontype)
                    {
                        case "Sale":
                            TranID = PosRecord.Transactionid;
                            TranDetail = PosRecord.Transactiondetail;
                            string saleresp = dt.SAPOrderTransactions(TranDetail.sh, BatchID, TranID);

                            break;
                        case "61":
                            MagentaCustomer MagCustomer61 = new MagentaCustomer();
                            BPCustomer SAPCustomer61 = new BPCustomer();
                            TranDetail = PosRecord.Transactiondetail;
                            TranID = PosRecord.Transactionid;
                            jsonstring = dt.MagentaStringToJSON(TranDetail.Sixtyone.customernvpdata);
                            MagCustomer61 = JsonConvert.DeserializeObject<MagentaCustomer>(jsonstring);
                            if (MagCustomer61.ChargeAccount == "1")
                                SAPCustomer61 = dt.SAPCustomer(MagCustomer61, BatchID, TranID, PosRecord.Transactiontype);
                            else
                                SAPCustomer61 = dt.SAPCustomer(MagCustomer61, BatchID, TranID, PosRecord.Transactiontype);

                            break;
                        case "62":
                            MagentaCustomer MagCustomer62 = new MagentaCustomer();
                            BPCustomer SAPCustomer62 = new BPCustomer();
                            TranDetail = PosRecord.Transactiondetail;
                            TranID = PosRecord.Transactionid;
                            jsonstring = dt.MagentaStringToJSON(TranDetail.Siztytwo.customeraddressnvpdata);
                            MagCustomer62 = JsonConvert.DeserializeObject<MagentaCustomer>(jsonstring);
                            SAPCustomer62 = dt.SAPCustomer(MagCustomer62, BatchID, TranID, PosRecord.Transactiontype);

                            break;
                        case "CF":
                            TranDetail = PosRecord.Transactiondetail;
                            break;
                        case "UP":
                            TranDetail = PosRecord.Transactiondetail;
                            break;
                        case "SA":
                            TranDetail = PosRecord.Transactiondetail;
                            break;
                    }
                }

                //string compresponse = CompleteFromMagenta(processname, BatchID);
                //dataContext.sp_ProcessSAP();

                Program.log.Info(string.Format("End GET Magenta Process: {0}", processname));

            }
            catch (Exception ex)
            {
                Program.log.Error(ex);
            }

            return "COMPLETE";
        }

        public void SAPOrdtoMag()
        {
            try
            {
                DataTransform dt = new DataTransform();
                Utilities ut = new Utilities();
                Stream str;
                byte[] xmlin;
                string strxml = "";
                string MagUserAuth = Properties.Settings.Default.MagRESTUserAuth;
                string MagPassAuth = Properties.Settings.Default.MagRESTPassAuth;
                List<Sale> xml = dt.SAPInvoicesToMagenta();
                string filename = "";
                string outputdir = string.Format("{0}{1}\\", AppContext.BaseDirectory, "XMLFiles");

                foreach (Sale newxml in xml)
                {

                    XmlSerializer s = new XmlSerializer(newxml.GetType());
                    StringWriter w = new StringWriter();
                    s.Serialize(w, newxml);
                    string strXML = w.ToString();
                    string docid = newxml.TranId;
                    filename = string.Format("OrdertoMag{0}_{1}.xml", docid.ToString(), DateTime.Now.ToString("yyyyMMddHHmmss"));

                    ut.CreateXMLFile(outputdir, filename, strXML);

                    //string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Sale xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><TranId>33</TranId><TranDate>04/08/2020 00:00:00</TranDate><CustomerCode>NT0000010134</CustomerCode><SaleBranchCode>903</SaleBranchCode><StockBranchCode>903</StockBranchCode><SaleLine><SaleLineId>0</SaleLineId><ProductCode>131358</ProductCode><Quantity>10.000000</Quantity><UnitSellingPrice>5.000000</UnitSellingPrice></SaleLine><SaleLine> <SaleLineId>1</SaleLineId><ProductCode>132580</ProductCode><Quantity>1.000000</Quantity><UnitSellingPrice>5.000000</UnitSellingPrice></SaleLine><SaleLine><SaleLineId>2</SaleLineId><ProductCode>142002</ProductCode><Quantity>1.000000</Quantity><UnitSellingPrice>6.990000</UnitSellingPrice></SaleLine><SaleLine><SaleLineId>3</SaleLineId><ProductCode>FREIGHT</ProductCode><Quantity>1.000000</Quantity><UnitSellingPrice>10.950000</UnitSellingPrice></SaleLine>"+
                    //"<TenderLine><TenderLineId>1</TenderLineId><TenderType>CASH</TenderType><ReferenceNumber></ReferenceNumber><TenderAmount>500</TenderAmount></TenderLine></Sale>";

                    xmlin = Utilities.GCompress(strXML);

                    using (var client = new EEEWebServiceAuthSoapClient())
                    {
                        try
                        {
                            //var response = await client.Sale_InsertAsync(GZip.Compress(GetXml()), "MWSTEST", "VIh5XnxglK");
                            var response = client.Sale_Insert(xmlin, MagUserAuth, MagPassAuth);

                            dataContext.sp_InvoiceResponse(docid, "SUCCESS");
                            Console.WriteLine($"Success: {response}");
                            Console.WriteLine($"Success: {response}");

                        }
                        catch (Exception e)
                        {
                            string msg = e.Message.Replace("'", "");
                            dataContext.sp_InvoiceResponse(docid, msg);
                            Console.WriteLine($"Error: {e}");
                            Console.WriteLine($"Error: {e}");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void SAPOrdtoMagMan(string filename)
        {
            try
            {
                DataTransform dt = new DataTransform();
                Utilities ut = new Utilities();
                Stream str;
                byte[] xmlin;
                string strxml = "";
                string MagUserAuth = Properties.Settings.Default.MagRESTUserAuth;
                string MagPassAuth = Properties.Settings.Default.MagRESTPassAuth;
                string fulldir = string.Format("{0}\\{1}", Directory.GetCurrentDirectory(), filename);
                string xmldatareturn = File.ReadAllText(@fulldir);
                //List<Sale> xml = dt.SAPInvoicesToMagenta();

                //foreach (Sale newxml in xml)
                //{

                //XmlSerializer s = new XmlSerializer(newxml.GetType());
                //StringWriter w = new StringWriter();
                //s.Serialize(w, newxml);
                //string strXML = w.ToString();

                string strXML = xmldatareturn;

                    //string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Sale xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><TranId>33</TranId><TranDate>04/08/2020 00:00:00</TranDate><CustomerCode>NT0000010134</CustomerCode><SaleBranchCode>903</SaleBranchCode><StockBranchCode>903</StockBranchCode><SaleLine><SaleLineId>0</SaleLineId><ProductCode>131358</ProductCode><Quantity>10.000000</Quantity><UnitSellingPrice>5.000000</UnitSellingPrice></SaleLine><SaleLine> <SaleLineId>1</SaleLineId><ProductCode>132580</ProductCode><Quantity>1.000000</Quantity><UnitSellingPrice>5.000000</UnitSellingPrice></SaleLine><SaleLine><SaleLineId>2</SaleLineId><ProductCode>142002</ProductCode><Quantity>1.000000</Quantity><UnitSellingPrice>6.990000</UnitSellingPrice></SaleLine><SaleLine><SaleLineId>3</SaleLineId><ProductCode>FREIGHT</ProductCode><Quantity>1.000000</Quantity><UnitSellingPrice>10.950000</UnitSellingPrice></SaleLine>"+
                    //"<TenderLine><TenderLineId>1</TenderLineId><TenderType>CASH</TenderType><ReferenceNumber></ReferenceNumber><TenderAmount>500</TenderAmount></TenderLine></Sale>";

                    xmlin = Utilities.GCompress(strXML);

                    using (var client = new EEEWebServiceAuthSoapClient())
                    {
                        try
                        {
                            //var response = await client.Sale_InsertAsync(GZip.Compress(GetXml()), "MWSTEST", "VIh5XnxglK");
                            var response = client.Sale_Insert(xmlin, MagUserAuth, MagPassAuth);

                            Console.WriteLine($"Success: {response}");
                            Console.WriteLine($"Success: {response}");

                        }
                        catch (Exception e)
                        {
                            Console.WriteLine($"Error: {e}");
                            Console.WriteLine($"Error: {e}");
                        }
                    }
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public void Dispose()
        {
            if (dataContext != null)
            {
                dataContext.Dispose();
            }
        }
    }
}
