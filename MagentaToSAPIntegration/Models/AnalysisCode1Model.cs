﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagentaToSAPIntegration.Models
{
    public class RecordAnalysisCode1
    {
        public string Analysis1code { get; set; }
        public string Description { get; set; }
    }

    public class AnalysisCode1
    {
        public bool Processnow { get; set; }
        public string Commssession { get; set; }
        public List<RecordAnalysisCode1> Records { get; set; }
    }
}
