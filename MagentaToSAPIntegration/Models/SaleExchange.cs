﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MagentaToSAPIntegration.Models
{

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [XmlTypeAttribute(AnonymousType = true)]
    [XmlRootAttribute(Namespace = "", IsNullable = false)]
    //[XmlRoot(Namespace = "", IsNullable = false)]
    public partial class Salexxx
    {

        private string tranIdField;

        private System.DateTime tranDateField;

        private string customerCodeField;

        private string saleBranchCodeField;

        private string stockBranchCodeField;

        private string masterCustomerCodeField;

        private string dINumberField;

        private string websiteCodeField;

        private string[] referralCardNumberField;

        private string[] cSROrderNumberField;

        private SaleLineType[] saleLineField;

        private TenderLineType[] tenderLineField;

        /// <remarks/>
        public string TranId
        {
            get
            {
                return this.tranIdField;
            }
            set
            {
                this.tranIdField = value;
            }
        }

        /// <remarks/>
        public System.DateTime TranDate
        {
            get
            {
                return this.tranDateField;
            }
            set
            {
                this.tranDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string CustomerCode
        {
            get
            {
                return this.customerCodeField;
            }
            set
            {
                this.customerCodeField = value;
            }
        }

        /// <remarks/>
        public string SaleBranchCode
        {
            get
            {
                return this.saleBranchCodeField;
            }
            set
            {
                this.saleBranchCodeField = value;
            }
        }

        /// <remarks/>
        public string StockBranchCode
        {
            get
            {
                return this.stockBranchCodeField;
            }
            set
            {
                this.stockBranchCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string MasterCustomerCode
        {
            get
            {
                return this.masterCustomerCodeField;
            }
            set
            {
                this.masterCustomerCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string DINumber
        {
            get
            {
                return this.dINumberField;
            }
            set
            {
                this.dINumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string WebsiteCode
        {
            get
            {
                return this.websiteCodeField;
            }
            set
            {
                this.websiteCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ReferralCardNumber", IsNullable = true)]
        public string[] ReferralCardNumber
        {
            get
            {
                return this.referralCardNumberField;
            }
            set
            {
                this.referralCardNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CSROrderNumber", IsNullable = true)]
        public string[] CSROrderNumber
        {
            get
            {
                return this.cSROrderNumberField;
            }
            set
            {
                this.cSROrderNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SaleLine")]
        public SaleLineType[] SaleLine
        {
            get
            {
                return this.saleLineField;
            }
            set
            {
                this.saleLineField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("TenderLine")]
        public TenderLineType[] TenderLine
        {
            get
            {
                return this.tenderLineField;
            }
            set
            {
                this.tenderLineField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class SaleLineType
    {

        private int saleLineIdField;

        private string productCodeField;

        private decimal quantityField;

        private decimal unitSellingPriceField;

        private decimal extendedDiscountField;

        private string discountReasonField;

        private string giftCardNumberField;

        private string couponCodeField;

        private string activationCodeField;

        /// <remarks/>
        public int SaleLineId
        {
            get
            {
                return this.saleLineIdField;
            }
            set
            {
                this.saleLineIdField = value;
            }
        }

        /// <remarks/>
        public string ProductCode
        {
            get
            {
                return this.productCodeField;
            }
            set
            {
                this.productCodeField = value;
            }
        }

        /// <remarks/>
        public decimal Quantity
        {
            get
            {
                return this.quantityField;
            }
            set
            {
                this.quantityField = value;
            }
        }

        /// <remarks/>
        public decimal UnitSellingPrice
        {
            get
            {
                return this.unitSellingPriceField;
            }
            set
            {
                this.unitSellingPriceField = value;
            }
        }

        /// <remarks/>
        public decimal ExtendedDiscount
        {
            get
            {
                return this.extendedDiscountField;
            }
            set
            {
                this.extendedDiscountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string DiscountReason
        {
            get
            {
                return this.discountReasonField;
            }
            set
            {
                this.discountReasonField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string GiftCardNumber
        {
            get
            {
                return this.giftCardNumberField;
            }
            set
            {
                this.giftCardNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string CouponCode
        {
            get
            {
                return this.couponCodeField;
            }
            set
            {
                this.couponCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string ActivationCode
        {
            get
            {
                return this.activationCodeField;
            }
            set
            {
                this.activationCodeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class TenderLineType
    {

        private int tenderLineIdField;

        private string tenderTypeField;

        private string referenceNumberField;

        private string giftCardPinField;

        private string cardGatewayTypeField;

        private System.Nullable<System.DateTime> cardGatewaySettlementDateField;

        private decimal tenderAmountField;

        /// <remarks/>
        public int TenderLineId
        {
            get
            {
                return this.tenderLineIdField;
            }
            set
            {
                this.tenderLineIdField = value;
            }
        }

        /// <remarks/>
        public string TenderType
        {
            get
            {
                return this.tenderTypeField;
            }
            set
            {
                this.tenderTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string ReferenceNumber
        {
            get
            {
                return this.referenceNumberField;
            }
            set
            {
                this.referenceNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string GiftCardPin
        {
            get
            {
                return this.giftCardPinField;
            }
            set
            {
                this.giftCardPinField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public string CardGatewayType
        {
            get
            {
                return this.cardGatewayTypeField;
            }
            set
            {
                this.cardGatewayTypeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(IsNullable = true)]
        public System.Nullable<System.DateTime> CardGatewaySettlementDate
        {
            get
            {
                return this.cardGatewaySettlementDateField;
            }
            set
            {
                this.cardGatewaySettlementDateField = value;
            }
        }

        /// <remarks/>
        public decimal TenderAmount
        {
            get
            {
                return this.tenderAmountField;
            }
            set
            {
                this.tenderAmountField = value;
            }
        }
    }
}
