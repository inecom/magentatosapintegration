﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace MagentaToSAPIntegration.Models
{
	[XmlRoot(ElementName = "GiftCardNumber")]
	public class GiftCardNumber
	{
		[XmlAttribute(AttributeName = "nil", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
		public string Nil { get; set; }
	}

	[XmlRoot(ElementName = "CouponCode")]
	public class CouponCode
	{
		[XmlAttribute(AttributeName = "nil", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
		public string Nil { get; set; }
	}

	[XmlRoot(ElementName = "SaleLine")]
	public class SaleLine
	{
		[XmlElement(ElementName = "SaleLineId")]
		public string SaleLineId { get; set; }
		[XmlElement(ElementName = "ProductCode")]
		public string ProductCode { get; set; }
		[XmlElement(ElementName = "Quantity")]
		public decimal Quantity { get; set; }
		[XmlElement(ElementName = "UnitSellingPrice")]
		public decimal UnitSellingPrice { get; set; }
		[XmlElement(ElementName = "ExtendedDiscount")]
		public decimal ExtendedDiscount { get; set; }
		[XmlElementAttribute(ElementName = "DiscountReason", IsNullable = true)]
		//[XmlElement(ElementName = "DiscountReason")]
		public string DiscountReason { get; set; }
		[XmlElementAttribute(ElementName = "GiftCardNumber", IsNullable = true)]
		//[XmlElement(ElementName = "GiftCardNumber")]
		public GiftCardNumber GiftCardNumber { get; set; }
		[XmlElementAttribute(ElementName = "CouponCode", IsNullable = true)]
		//[XmlElement(ElementName = "CouponCode")]
		public CouponCode CouponCode { get; set; }
		[XmlElementAttribute(ElementName = "ActivationCode", IsNullable = true)]
		//[XmlElement(ElementName = "ActivationCode")]
		public string ActivationCode { get; set; }
	}

	[XmlRoot(ElementName = "CardGatewayType")]
	public class CardGatewayType
	{
		[XmlAttribute(AttributeName = "nil", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
		public string Nil { get; set; }
	}

	[XmlRoot(ElementName = "CardGatewaySettlementDate")]
	public class CardGatewaySettlementDate
	{
		[XmlAttribute(AttributeName = "nil", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
		public string Nil { get; set; }
	}

	[XmlRoot(ElementName = "TenderLine")]
	public class TenderLine
	{
		[XmlElement(ElementName = "TenderLineId")]
		public int TenderLineId { get; set; }
		[XmlElement(ElementName = "TenderType")]
		public string TenderType { get; set; }
		[XmlElementAttribute(ElementName = "ReferenceNumber", IsNullable = true)]
		//[XmlElement(ElementName = "ReferenceNumber")]
		public string ReferenceNumber { get; set; }
		[XmlElementAttribute(ElementName = "GiftCardPin", IsNullable = true)]
		//[XmlElement(ElementName = "GiftCardPin")]
		public string GiftCardPin { get; set; }
		[XmlElementAttribute(ElementName = "CardGatewayType", IsNullable = true)]
		//[XmlElement(ElementName = "CardGatewayType")]
		public CardGatewayType CardGatewayType { get; set; }
		[XmlElementAttribute(ElementName = "CardGatewaySettlementDate", IsNullable = true)]
		//[XmlElement(ElementName = "CardGatewaySettlementDate")]
		public CardGatewaySettlementDate CardGatewaySettlementDate { get; set; }
		[XmlElement(ElementName = "TenderAmount")]
		public decimal TenderAmount { get; set; }
	}

	[XmlRoot(ElementName = "Sale")]
	public class Sale
	{
		[XmlElement(ElementName = "TranId")]
		public string TranId { get; set; }
		[XmlElement(ElementName = "TranDate")]
		public DateTime TranDate { get; set; }
		[XmlElementAttribute(ElementName = "CustomerCode", IsNullable = true)]
		//[XmlElement(ElementName = "CustomerCode")]
		public string CustomerCode { get; set; }
		[XmlElement(ElementName = "SaleBranchCode")]
		public string SaleBranchCode { get; set; }
		[XmlElement(ElementName = "StockBranchCode")]
		public string StockBranchCode { get; set; }
		[XmlElementAttribute(ElementName = "MasterCustomerCode", IsNullable = true)]
		//[XmlElement(ElementName = "MasterCustomerCode")]
		public string MasterCustomerCode { get; set; }
		[XmlElementAttribute(ElementName = "DINumber", IsNullable = true)]
		//[XmlElement(ElementName = "DINumber")]
		public string DINumber { get; set; }
		[XmlElementAttribute(ElementName = "WebsiteCode", IsNullable = true)]
		//[XmlElement(ElementName = "WebsiteCode")]
		public string WebsiteCode { get; set; }
		[XmlElementAttribute(ElementName = "ReferralCardNumber", IsNullable = true)]
		//[XmlElement(ElementName = "ReferralCardNumber")]
		private string[] referralCardNumberField;
		[XmlElementAttribute(ElementName = "CSROrderNumber", IsNullable = true)]
		//[XmlElement(ElementName = "CSROrderNumber")]
		private string[] cSROrderNumberField;
		[XmlElement(ElementName = "SaleLine")]
		public List<SaleLine> SaleLine { get; set; }
		[XmlElement(ElementName = "TenderLine")]
		public List<TenderLine> TenderLine { get; set; }
		[XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsi { get; set; }
	}

	[XmlRoot(ElementName = "Sales")]
	public class Sales
	{
		[XmlElement(ElementName = "Sale")]
		public List<Sale> Sale { get; set; }
		[XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
		public string Xsi { get; set; }
	}
}
